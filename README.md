# Installation
- composer require propertywebmasters/discover-theme

# Publish Assets (Required)
- php artisan vendor:publish --tag=discover-theme-assets

# Publish Blade Files
- php artisan vendor:publish --tag=discover-theme-views
