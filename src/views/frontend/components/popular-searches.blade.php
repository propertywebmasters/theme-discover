@if ($popularSearches->count() > 0)
    <section id="popular-searches" class="py-12 px-4">
        <div class="container mx-auto sm:px-4 md:px-0">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                @foreach($popularSearches as $search)
                    <div class="rounded-lg popular-search-location relative overflow-hidden" style="background-size: cover; background-position: center center; height: 365px; background-image: url('{{ assetPath($search->image) }}'); background-repeat: no-repeat;">
                        <div class="popular-search-location-item absolute top-0 left-0 h-full w-full text-center px-8" style="background: rgba(0, 0, 0, 0.4); display: flex; justify-content: center; align-items: center;">
                            <div class="absolute bottom-0 lg:-bottom-28 left-0 pl-8 md:pl-10 pb-12 text-left transition-all">
                                <p class="text-white text-2xl md:text-3xl header-text mb-8 lg:mb-12">{!! $search->title !!}</p>
                                <a href="{{ $search->url }}" class="rounded-3xl text-sm md:text-base text-white border border-white py-2 px-5 hover:bg-white hover:text-black transition-all uppercase inline-block">{{ trans('generic.view_more') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
