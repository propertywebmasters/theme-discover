
@php
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $useSettingsCache = config('platform-cache.site_settings_enabled');
    $siteName = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_NAME['key'], $useSettingsCache);
    $price = html_entity_decode($property->displayPrice());
    $whatsappTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_WHATSAPP_NUMBER['key']);
    $message = 'Hello, I would like to learn more information about this property you have listed on '. $siteName .', REFERENCE: '. $property->displayReference() . ', TYPE: ' . $property->propertyType->name . ', PRICE: ' . $price . ' - ' . url('/');
@endphp

<a href="https://api.whatsapp.com/send?phone={{ urlencode($whatsappTel) }}&text={{ urlencode($message) }}" class="rounded-full text-white h-9 sm:h-12 px-4 sm:px-9 cursor-pointer inline-block" style="background-color: #25D366;" target="_BLANK">
    <div class="flex items-center h-full">
        <span class="text-base uppercase font-bold">{{ trans('button.click_to_whatsapp') }}</span>
    </div>
</a>
