<div id="property-note-modal" class="outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mt-6 mx-auto max-w-xl px-3 sm:px-0">

        <!--content-->
        <div class="border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-8 pb-12 px-12 flex-auto">
                <button data-target="property-note-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('button.add_property_note') }}</h3>

                <form action="{{ localeUrl('/property/'.$property->url_key.'/note') }}" method="post" enctype="application/x-www-form-urlencoded">
                    <textarea class="border w-full h-32 p-2 focus:outline-none" name="note" placeholder="{{ trans('label.your_note') }}"></textarea>
                    <div class="mt-6 text-center">
                        <button type="submit" role="button"
                                class="rounded-full primary-bg block w-full text-center py-4 px-10 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">
                            {{ trans('label.create_property_note') }}
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
