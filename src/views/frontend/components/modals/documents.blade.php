@php
    $class = 'mx-auto w-screen overflow-x-hidden mt-2 lg:mt-20 xl:w-4/5 h-5/6 px-3 sm:px-0';
    $mapClass = 'w-full h-118';
@endphp
<div id="property-documents-modal" class="h-full outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative mt-6 w-auto {{ $class }}">

        <!--content-->
        <div class="border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-3 px-2 pb-2 flex-auto">
                <button data-target="property-documents-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('header.documents') }}</h3>

                <div class="pt-4 text-center">
                    <div class="grid grid-cols-2 lg:grid-cols-{{ $property->documents->count() }} gap-2">
                        @foreach($property->documents as $document)
                            @php
                            $caption = empty($document->caption) ? '&nbsp;' : $document->caption;
                            @endphp
                            <div>
                                <span class="block text-lg font-medium">{!! $caption !!}</span>
                                <span class="block text-xs">File Type: {{ $document->type }}</span>
                                <a class="block" href="{{ assetPath($document->filepath) }}" target="_blank" title="{{ trans('generic.download_file') }}">
                                    <img src="{{ themeImage('icons/file.svg') }}" class="block svg-inject h-20 inline-block fill-current stroke-current primary-text" alt="File">
                                </a>
                                <a class="block" href="{{ assetPath($document->filepath) }}" target="_blank" title="{{ trans('generic.download_file') }}">
                                    {{ trans('generic.download_file') }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
