<div id="cookie-modal" class="outside-click show overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mt-6 mx-auto max-w-xl px-3 sm:px-0">

        <!--content-->
        <div class="modal-content-holder border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative p-4 lg:pt-8 lg:pb-12 lg:px-12 flex-auto">

                <button data-target="cookie-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>

                <div id="cookie-policy" class="">
                    <div>
                        <h3 class="text-2xl leading-relaxed text-center font-black pb-4">{{ trans('cookie.your_cookies') }}</h3>
                        <p>{{ trans('cookie.policy') }}</p>
                    </div>

                    <form id="cookie-policy-form" class="hidden" action="{{ route('cookies.store') }}" method="POST">
                        @csrf

                        @foreach ($customScripts as $customScript)
                            @if (!$customScript->is_mandatory)
                                <input type="hidden" name="{{ $customScript->cookie_name }}" value="1">
                            @endif
                        @endforeach

                        <input type="hidden" name="cookie-policy" value="true">
                    </form>

                    <div class="flex justify-center mt-5">
                        @if($customScripts->count() > 0)
                            <div class="mr-2 w-1/2 text-center">
                                <a class="bg-gray-400 py-2 px-4 text-white block cookie-action-box-button rounded-full" data-toggle="cookie-preference" href="javascript:;">{{ trans('cookie.review') }}</a>
                            </div>
                        @endif
                        <div class="ml-2 w-1/2 text-center">
                            <a class="primary-bg py-2 px-4 text-white block cookie-policy-form-submit-button rounded-full" href="javascript:;">{{ trans('cookie.accept_policy') }}</a>
                        </div>
                    </div>
                </div>

                <div id="cookie-preference" class="hidden">
                    <h3 class="text-2xl leading-relaxed text-center font-black pb-4">{{ trans('cookie.cookie_preference') }}</h3>

                    <form id="cookie-preference-form" action="{{ route('cookies.store') }}" method="POST">
                        @csrf

                        <div>
                            @foreach ($customScripts as $customScript)
                                @if (!$customScript->is_mandatory)
                                    <div class="flex p-1">
                                        <div class="w-1/2">
                                            <label for="{{ $customScript->cookie_name }}">{{ $customScript->name }}</label>
                                        </div>
                                        <div class="w-1/2 text-right">
                                            <select id="{{ $customScript->cookie_name }}" name="{{ $customScript->cookie_name }}">
                                                <option value="1">{{ trans('cookie.on') }}</option>
                                                <option value="0">{{ trans('cookie.off') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <input type="hidden" name="cookie-policy" value="true">
                    </form>

                    <div class="flex justify-center mt-5">
                        <div class="mr-2 w-1/2 text-center">
                            <a class="bg-gray-400 py-2 px-4 text-white block cookie-action-box-button rounded-full" data-toggle="cookie-policy" href="javascript:;">{{ trans('cookie.cancel') }}</a>
                        </div>
                        <div class="ml-2 w-1/2 text-center">
                            <a class="primary-bg py-2 px-4 text-white block cookie-preference-form-submit-button rounded-full" href="javascript:;">{{ trans('cookie.submit') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
