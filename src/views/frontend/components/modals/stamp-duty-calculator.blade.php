<div id="stamp-duty-calculator-modal" class="outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mt-6 mx-auto max-w-xl px-3 sm:px-0">

        <!--content-->
        <div class="relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-8 flex-auto">
                <button data-target="stamp-duty-calculator-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('header.stamp_duty_calculator') }}</h3>


                <!-- start -->

                <div class="shadow-md bg-white p-3 md:p-8">
                    <!-- Tab links -->
                    <div class="text-right">
                        <div class="tab relative z-10 inline-block bg-white">
                            <button class="tablinks single primary-bg text-white py-3 px-5 border active" data-target="single-property-tab">Single Property</button>
                            <button class="tablinks additional bg-gray-300 py-3 px-5 border" data-target="additional-property-tab">Additional Property</button>
                        </div>
                    </div>
                    <div class="relative z-0 mb-16 mt-0.5">
                        <input type="number" name="price" step="1000" placeholder="eg: 275000" required="" class="w-full px-3 py-2 border h-14 -mt-1 focus:outline-none">
                    </div>

                    <!-- Tab content -->
                    <div id="single-property-tab" class="tabcontent">
                        <table class="bg-gray-100 w-full p-0">
                            <thead>
                            <tr>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">TAX BAND</th>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">%</th>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">TAXABLE SUM
                                </th>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">TAX</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="band-a">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">Less than &pound;300k</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">0 (2/5)*</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-b">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">&pound;300k - &pound;500k</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">5</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-c">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">&pound;500k - &pound;925k</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">5</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-d">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">&pound;925 - &pound;1.5m</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">10</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-e">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">Over &pound;1.5m</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">12</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="effective-rate">
                                <td colspan="2" class="border-4 border-white bg-gray-600 px-3 py-3 text-white">Effective Rate</td>
                                <td colspan="2" class="effective-rate border-4 border-white bg-gray-600 bg-gray-600 px-3 py-3 text-white">0.00%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="additional-property-tab" class="tabcontent hidden">
                        <table class="bg-gray-100 w-full p-0">
                            <thead>
                            <tr>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">TAX BAND</th>
                                <th class="border-4 border-white bg-gray-100 text-left px-3 py-3">%</th>
                                <th class="taxable-sum border-4 border-white bg-gray-100 text-left px-3 py-3">TAXABLE SUM</th>
                                <th class="tax border-4 border-white bg-gray-100 text-left px-3 py-3">TAX</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="band-a">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">Less than &pound;500k</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">3</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-b">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">&pound;500k - &pound;925k</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">8</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-c">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">&pound;925k - &pound;1.5m</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">13</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="band-d">
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">Over &pound;1.5m</td>
                                <td class="border-4 border-white bg-gray-100 px-3 py-3">15</td>
                                <td class="taxable-sum border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                                <td class="tax border-4 border-white bg-gray-100 px-3 py-3">£0</td>
                            </tr>
                            <tr class="effective-rate">
                                <td colspan="2" class="border-4 border-white bg-gray-600 px-3 py-3 text-white">Effective Rate</td>
                                <td colspan="2" class="effective-rate border-4 border-white bg-gray-600 bg-gray-600 px-3 py-3 text-white">0.00%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="w-full">
                        <button id="calculate-stamp-duty" class="rounded-full ml-1 primary-bg block text-center py-4 text-xs sm:text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer hover-lighten" style="width: 98.5%;">
                            Calculate
                        </button>
                    </div>
                </div>

                <div class="total-stamp-duty-wrapper hidden bg-gray-200 pb-12 px-5 text-center relative">
                    <img src="{{ themeImage('icons/triangle.png') }}" alt="triangle" class="absolute t-0 left-1/2 transform -translate-x-1/2">
                    <p class="text-lg  leading-normal pt-12 ">Your stamp duty:</p>
                    <span class="total-stamp-duty block text-4xl mt-1 font-semibold uppercase primary-text">&nbsp;</span>
                </div>


            </div>
        </div>
    </div>
</div>
