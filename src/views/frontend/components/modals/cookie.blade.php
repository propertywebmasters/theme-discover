@if ($customScripts->count() >= 0)

    @if (hasFeature(\App\Models\TenantFeature::FEATURE_USE_BOX_COOKIE_MODAL))
        @include(themeViewPath('frontend.components.modals.designs.cookie-box'))
    @else
        @include(themeViewPath('frontend.components.modals.designs.cookie-sticky-footer'))
    @endif

@endif
