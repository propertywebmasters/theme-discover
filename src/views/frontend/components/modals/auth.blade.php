<div id="preauth-modal" class="modal-box outside-click shadow hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mt-6 mx-auto max-w-xl px-3 sm:px-0" style>
        <!--content-->
        <div class="relative border-0 shadow-lg relative flex flex-col w-full outline-none focus:outline-none">

            <button data-target="preauth-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
            </button>

            <div class="preauth-modal-wrapper">

                <div class="modal-box-header text-center py-8 xl:px-16 bg-gray-200">
                    <h3 class="text-xl lg:text-3xl leading-relaxed text-center header-text">{!! translatableContent('auth-modal', 'modal-title') !!}</h3>

                    <div class="relative pt-8 px-4 flex-auto">

                        <div class="grid grid-cols-2 gap-6">
                            <div class="text-center">
                                <img class="svg-inject block mx-auto w-12 h-12 primary-text stroke-current fill-current" src="{{ themeImage('icons/heart.svg') }}">
                                <span class="block text-sm text-gray-500 mt-2 lg:px-4">{!! translatableContent('auth-modal', 'header-text-1') !!}</span>
                            </div>

                            <div class="text-center">
                                <img class="svg-inject block mx-auto w-12 h-12 primary-text stroke-current fill-current" src="{{ themeImage('icons/bell.svg') }}">
                                <span class="block text-sm text-gray-500 mt-2 lg:px-4">{!! translatableContent('auth-modal', 'header-text-2') !!}</span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-box-footer bg-white text-center pt-8 pb-4 xl:pb-12 px-4 xl:px-16">
                    <h4 class="text-xl lg:text-3xl leading-1 text-center m-0 p-0 header-text">{!! translatableContent('auth-modal', 'header-main', null, null, ) !!}</h4>
                    <span class="text-gray-700">{!! translatableContent('auth-modal', 'header-sub') !!}</span>

                    @php
                    $dataLocale = app()->getLocale() !== 'en' ? app()->getLocale() : ''
                    @endphp

                    <div class="preauth-replaceable">
                        <form id="preauth-form" class=" py-2" action="javascript:" method="post" enctype="application/x-www-form-urlencoded" data-locale="{{ app()->getLocale() }}">

                            <input id="preauth-email" type="email" name="email" placeholder="{{ trans('auth.email_address') }}" class="block bg-gray-200 text-gray-900 block w-full my-3 p-2" value="" required>
                            <button type="submit" role="button" class="rounded-full w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">
                                {{ trans('auth.continue') }} &rarr;
                            </button>
                            @csrf
                            <input type="hidden" name="current_url" value="{{ url()->current() }}">
                        </form>

                        <a id="forgotten-password" class="block mt-4 cursor-pointer" href="javascript:">{!! translatableContent('login-modal', 'modal-forgotten-password-text') !!}</a>

                        @php
                            use App\Models\SocialAuth;$googleType = SocialAuth::TYPE_GOOGLE;
                            $facebookType = SocialAuth::TYPE_FACEBOOK;
                            $googleAuth = getSocialAuth($googleType);
                            $facebookAuth = getSocialAuth($facebookType)
                        @endphp

                        @if ($googleAuth || $facebookAuth)
                            <span class="m-auto py-4 text-center block text-gray-600">{{ trans('auth.or') }}</span>
                        @endif

                        @if ($googleAuth)
                            <a class="block border border-gray-300 my-4 p-4 hover:border-gray-500 cursor-pointer" href="{{ route('social-auth-login', [$googleType]) }}">
                                <img class="inline-block h-6 w-6 mr-2" src="{{ themeImage('icons/google-icon.svg') }}" alt="Google" loading="lazy"> {{ trans('auth.continue_with_google') }}
                            </a>
                        @endif

                        @if ($facebookAuth)
                            <a class="block border border-gray-300 p-4 hover:border-gray-500 cursor-pointer" href="{{ route('social-auth-login', [$facebookType]) }}">
                                <img class="inline-block h-6 w-6 mr-2" src="{{ themeImage('icons/facebook-icon.svg') }}" alt="Facebook" loading="lazy"> {{ trans('auth.continue_with_facebook') }}
                            </a>
                        @endif

                    </div>

                    <div class="preauth-loader hidden">
                        <div class=" pt-8 w-32 mx-auto text-center primary-text">
                            @include('frontend.components.loader')
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
