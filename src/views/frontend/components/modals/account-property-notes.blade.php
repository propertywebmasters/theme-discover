@php
    $class = 'mx-auto overflow-x-hidden mt-2 lg:mt-20';
@endphp
<div id="account-property-notes-modal-{{ $property->uuid }}"
     class="h-full outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative mt-6 w-1/2 {{ $class }}">

        <!--content-->
        <div class="border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-3 px-2 pb-2 flex-auto">
                <button data-target="account-property-notes-modal-{{ $property->uuid }}" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('account.property_notes') }}</h3>
                <div class="px-4">
                    @foreach($property->notes as $propertyNote)
                    <div class="grid grid-cols-1 gap-2">
                        <div class="border my-2 p-4">
                            <div class="text-right">
                                <a class="inline-block text-lg md:text-base" href="/account/notes/{{ $propertyNote->uuid }}/delete" onclick="return confirm('Are you sure you want to delete this item?');" data-property="{{ $property->uuid }}">
                                    <img src="{{ themeImage('icons/trash.svg') }}" class="h-5 stroke-current  cursor-pointer" alt="Delete" loading="lazy">
                                </a>
                            </div>
                            <span class="block text-base">{!! $propertyNote->note !!}</span>
                            <span class="block text-xs primary-text mt-2">Saved {{ $propertyNote->created_at->format('jS F Y - H:i') }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
