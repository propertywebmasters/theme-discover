<div id="become-member-modal" class="overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center shadow">
    <div class="relative w-auto mt-6 mx-auto max-w-4xl">
        <!--content-->
        <div class="border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative flex-auto">
                <button data-target="book-viewing-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <div class="grid grid-cols-2 gap-2">
                    <div style="background-repeat: no-repeat; background-image: url('https://media.architecturaldigest.com/photos/5eac5fa22105f13b72dede45/3:2/w_1599,h_1066,c_limit/111LexowAve_Aug18-1074.jpg')" class="center-cover-bg">&nbsp;</div>
                    <div class="p-16">
                        <h3 class="text-4xl font-medium pb-4">Become a member today?</h3>
                        <p class="font-medium">Benefit from a range of extra's if you register an account.</p>
                        <ul class="list-disc list-inside my-6">
                            <li class="py-1">Create alerts for new listings</li>
                            <li class="py-1">Share properties you love</li>
                            <li class="py-1">Download property PDFs</li>
                            <li class="py-1">First access to new listings</li>
                        </ul>

                        <button type="submit" role="button" class="w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">
                            Sign up today
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
