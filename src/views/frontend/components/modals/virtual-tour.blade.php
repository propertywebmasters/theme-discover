@php
    $class = 'mx-auto w-screen overflow-x-hidden mt-2 lg:mt-20 xl:w-4/5 h-5/6 px-3 sm:px-0';
    $tourUrl = getEmbedUrlForPropertyVideo($property->virtual_tour_url)
@endphp
<div id="property-virtual-tour-modal" class="h-full outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative mt-6 w-auto {{ $class }}">

        <!--content-->
        <div class="border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-3 px-2 pb-2 flex-auto">
                <button data-target="property-virtual-tour-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('header.virtual_tour') }}</h3>
                <iframe class="h-142 w-full" src="{{ $tourUrl }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</div>
