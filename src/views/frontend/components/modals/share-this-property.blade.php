<div id="share-this-property-modal" class="outside-click hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mt-6 mx-auto max-w-xl px-3 sm:px-0">

        <!--content-->
        <div class="modal-box-body border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div class="relative pt-8 pb-12 px-12 flex-auto">
                <button data-target="share-this-property-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
                    <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                </button>
                <h3 class="text-2xl leading-relaxed text-center font-black pb-4 header-text">{{ trans('search.share_this_property') }}</h3>
                @include(themeViewPath('frontend.forms.share-this-property'))
            </div>
        </div>
    </div>
</div>
