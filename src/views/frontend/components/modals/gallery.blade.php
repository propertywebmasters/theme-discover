@php
    $galleryHasViewingForm = hasFeature(\App\Models\TenantFeature::FEATURE_GALLERY_HAS_VIEWING_FORM);
    $propertyImageClasses = $galleryHasViewingForm ? 'col-span-4 xl:col-span-3 relative' : 'col-span-4';
@endphp

<div id="gallery-modal" class="outside-click hidden fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center">
    <div class="relative w-auto mx-auto grid grid-cols-4">
        <div class="{{ $propertyImageClasses }} h-screen overflow-x-hidden overflow-y-hidden">
            <!--content-->
            <div id="property-image-container" class="shadow-lg relative flex flex-col outline-none focus:outline-none top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 sm:h-fit-content md:h-4/5 w-fit-content">
                <div class="absolute inset-x-0 bottom-0 shadow-xl -mt-1 w-auto text-center text-sm mb-4 opacity-50 bg-white" title="">
                    <span id="gallery-caption" class="text-2xl sm:text-sm"></span>
                </div>

                @if ($property->images->first() !== null)
                    @php
                    // @todo fix this so that it stops stretching images
                    $css = 'max-h-screen w-auto max-w-screen';
                    @endphp
                    <img id="gallery-image-focus" class="next-image cursor-pointer select-none focus:outline-none object-contain h-full w-auto" src="{{ getPropertyImage($property->images->first()->filepath, 'lg') }}" alt="slider" loading="lazy">
                @endif

                <div class="absolute z-50 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 loading-indicator"><p class="text-white">Loading...</p></div>
            </div>

            @if ($galleryHasViewingForm)
                <div>
                    <div class="close-gallery absolute right-0 top-0 mr-4 mt-4 cursor-pointer xl:hidden" title="Close">
                        <i class="text-white w-8 h-8 z-50" data-feather="x-circle"></i>
                    </div>
                    <div class="prev-image absolute left-0 top-1/2 ml-4 -mt-4 cursor-pointer" title="Previous">
                        <i class="text-white w-8 h-8 z-50" data-feather="arrow-left-circle"></i>
                    </div>
                    <div class="next-image absolute right-0 top-1/2 mr-4 -mt-4 cursor-pointer" title="Next">
                        <i class="text-white w-8 h-8 z-50" data-feather="arrow-right-circle"></i>
                    </div>
                    <div class="absolute image-numbers-wrapper bottom-0 left-1/2 transform -translate-x-1/2 mb-4">
                        <p class="text-white select-none">
                            <span class="image-number"></span> of <span class="image-numbers-total"></span>
                        </p>
                    </div>
                </div>
            @endif
        </div>

        @if ($galleryHasViewingForm)
            <div class="col-span-1 h-screen relative hidden xl:block">
                <div class="close-gallery absolute right-0 top-0 mr-4 mt-4 cursor-pointer" title="Close">
                    <i class="primary-text w-8 h-8 z-50" data-feather="x"></i>
                </div>

                @include(themeViewPath('frontend.forms.gallery-book-viewing'))
            </div>
        @endif
    </div>

    @if (!$galleryHasViewingForm)
        <div>
            <div class="close-gallery absolute right-0 top-0 mr-4 mt-4 cursor-pointer" title="Close">
                <i class="text-white w-8 h-8 z-50" data-feather="x-circle"></i>
            </div>
            <div class="prev-image absolute left-0 top-1/2 ml-4 -mt-4 cursor-pointer" title="Previous">
                <i class="text-white w-8 h-8 z-50" data-feather="arrow-left-circle"></i>
            </div>
            <div class="next-image absolute right-0 top-1/2 mr-4 -mt-4 cursor-pointer" title="Next">
                <i class="text-white w-8 h-8 z-50" data-feather="arrow-right-circle"></i>
            </div>
            <div class="absolute image-numbers-wrapper bottom-0 left-1/2 transform -translate-x-1/2 mb-4">
                <p class="text-white select-none">
                    <span class="image-number"></span> of <span class="image-numbers-total"></span>
                </p>
            </div>
        </div>
    @endif
</div>

