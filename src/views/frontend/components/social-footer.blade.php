@php
$service = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
$socialAccounts = $service->get();

$siteName = app()->make(\App\Services\SiteSetting\Contracts\SiteSettingServiceInterface::class)->retrieve(\App\Models\SiteSetting::SETTING_SITE_NAME['key'], config('platform-cache.site_settings_enabled'));

$socialCount = 0;
if ($socialAccounts !== null) {
    $networks = collect(\App\Models\SocialAccount::SOCIAL_SYSTEMS)->pluck('name')->toArray();

    foreach ($networks as $network) {
        if ($socialAccounts->$network !== null) {
            $socialCount++;
        }
    }
}
@endphp

<div class="container mx-auto py-9 md:py-7 px-4 md:px-0">
    <div class="footer-section flex md:justify-between flex-col md:flex-row">
        <div class="text-center md:text-left hidden md:block">
            <p class="text-xs tracking-tight">Copyright {{ $siteName }} {{ date('Y') }}. All rights reserved.
            </p>
        </div>
        @if($socialCount > 0)
            <div class="flex items-center px-0 md:px-5 py-3 md:py-0 mx-auto md:mx-0">
                @foreach($networks as $network)
                    @if($socialAccounts->$network === null)
                        @continue
                    @endif
                    <a href="{{ $socialAccounts->$network }}" class="pr-4 inline-block" target="_BLANK">
                        <img src="{{ themeImage($network.'.svg') }}" class="svg-inject nav-text fill-current" alt="{{ $network }}">
                    </a>
                @endforeach
            </div>
        @endif
        <div class="text-left mx-auto md:mx-0 md:hidden ">
            <p class="text-xs tracking-tight">&copy; Copyright {{ $siteName }} {{ date('Y') }}, All Rights Reserved.</p>
        </div>
        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHOW_FOOTER_PW_LINK))
            <div class="text-left mx-auto md:mx-0 ">
                <p class="text-xs tracking-tight pw-copyright">
                    <a class="text-xs tracking-tight" href="{{ config('footer.pw_website_link') }}" target="_BLANK" rel="noopener">{{ config('footer.pw_website_link_text') }}</a> by Property Webmasters
                </p>
            </div>
        @endif
    </div>
</div>
