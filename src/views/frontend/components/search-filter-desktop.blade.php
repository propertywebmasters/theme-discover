<section class="bg-gray-100 pt-2 md:pt-6 relative z-10">
    <form id="listings-search" class="listings-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">
        <div class="container mx-auto px-4 relative z-20">
            <div class="relative grid grid-cols-2 lg:grid-cols-{{ $primaryGridCols }} w-full gap-2 lg:gap-0">

                @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                    <div class="bg-white p-4 col-span-2 lg:col-span-1 rounded-l-0 lg:rounded-l-full">
                        <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-white text-base lg:text-sm xl:text-base appearance-none  select-carat">
                            <option value="sale" @if ($searchRequest->request->get('type') === 'sale') selected @endif>{{ trans('label.sale') }}</option>
                            <option value="rental" @if ($searchRequest->request->get('type') === 'rental') selected @endif>{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental"
                                        @if ($searchRequest->request->get('type') === 'rental' && $searchRequest->request->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental"
                                        @if ($searchRequest->request->get('type') === 'rental' && $searchRequest->request->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                            @endif
                            @if($hasStudentRentals)
                                <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                            @endif
                            @if($hasHolidayRentals)
                                <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                            @endif
                        </select>
                    </div>
                @else
                    @if($searchControls->tender_type === 'sale_only')
                        <input id="search_type" type="hidden" name="type" value="sale">
                    @elseif($searchControls->tender_type === 'rental_only')
                        <div class="bg-white p-4 col-span-2 lg:col-span-1 rounded-l-0 lg:rounded-l-full">
                            <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-white text-base lg:text-sm xl:text-base appearance-none  select-carat">
                                <option value="rental" @if ($searchRequest->request->get('type') === 'rental') selected @endif>{{ trans('label.rental') }}</option>
                                @if($hasShortTermRentals)
                                    <option value="short_term_rental"
                                            @if ($searchRequest->request->get('type') === 'rental' && $searchRequest->request->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                                @endif
                                @if($hasLongTermRentals)
                                    <option value="long_term_rental"
                                            @if ($searchRequest->request->get('type') === 'rental' && $searchRequest->request->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                @endif
                                @if($hasStudentRentals)
                                    <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                @endif
                                @if($hasHolidayRentals)
                                    <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                @endif
                            </select>
                        </div>
                    @endif
                @endif

                @php
                    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';
                    $roundedClass = '';
                    $inputClass = 'w-full autocomplete-location lg:pl-2 border-l-0 pl-0 placeholder-black focus:outline-none text-base lg:text-sm xl:text-base ';

                    if (isset($searchControls->property_type) && !$searchControls->property_type && isset($searchControls->min_price) && !$searchControls->min_price && isset($searchControls->max_price) && !$searchControls->max_price) {
                        $roundedClass .= 'lg:rounded-r-full rounded-r-none ';
                    }

                    if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
                        $roundedClass .= 'lg:rounded-l-full rounded-l-none ';
                    } else {
                        $inputClass .= 'lg:border-l lg:border-r';
                    }
                @endphp

                {{-- @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
                    <div class="lg:col-span-2 bg-white p-4 col-span-2 relative {{ $roundedClass }}">
                        @include(themeViewPath('frontend.components.location-datalist-refine-desktop'))
                    </div>
                @else
                    @if(isset($searchControls->location) && $searchControls->location)
                        <div class="lg:col-span-2 bg-white p-4 col-span-2 relative {{ $roundedClass }}">
                            <input id="search_location" type="text"
                                   class="{{ $inputClass }}"
                                   name="location" placeholder="{{ trans('placeholder.search_location') }}" value="{{ $locationValue }}" autocomplete="off"
                                   data-search-results-container="search-results-container">
                            <input type="hidden" name="location_url" value="">
                            <div id="autocomplete-results" class="click-outside absolute top-12 w-full z-30"
                                 data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                                <ul class="search-results-container"></ul>
                            </div>
                        </div>
                    @endif
                @endif --}}

                @include(themeViewPath('frontend.components.location-search'), [
                    'refinedSearch' => true,
                    'additionalClasses' => $roundedClass,
                    'inputClasses' => $inputClass,
                    'value' => $locationValue,
                ])



                @php
                    // define default visibility for select dropdowns
                    $salePriceHidden = in_array($searchRequest->request->get('type'), ['rental', 'short-term-rental', 'long-term-rental']) ? 'hidden' : '';
                    $rentalPriceHidden = !in_array($searchRequest->request->get('type'), ['rental', 'short-term-rental', 'long-term-rental']) ? 'hidden' : '';
                    $currentCurrency = currentCurrency()
                @endphp

                @if(isset($searchControls->property_type) && $searchControls->property_type)
                    @php
                        $roundedClass = '';
                        $inputClass = 'w-full block focus:outline-none -ml-1 bg-white lg:pl-2 border-l-0 pl-0 text-base lg:text-sm xl:text-base appearance-none select-carat rounded-none border-b-0 border-t-0 border-right-0 ';

                        if (isset($searchControls->min_price) && !$searchControls->min_price && isset($searchControls->max_price) && !$searchControls->max_price) {
                            $roundedClass .= 'lg:rounded-r-full lg:rounded-r-none ';
                        }

                        if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only' && isset($searchControls->location) && !$searchControls->location) {
                            $roundedClass .= 'lg:rounded-l-full rounded-l-none ';
                        } else {
                            $inputClass .= 'border-l lg:border-l ';
                        }
                    @endphp

                    <div class="bg-white p-4 col-span-2 lg:col-span-1 {{ $roundedClass }}">
                        @include(themeViewPath('frontend.components.multi-select-refine-desktop'))
                    </div>
                @endif

                @if(isset($searchControls->min_price) && $searchControls->min_price)
                    @php
                        $roundedClass = '';
                        $inputClass = 'w-full block focus:outline-none -ml-1 bg-white lg:pl-2 border-l-0 pl-0 text-base lg:text-sm xl:text-base appearance-none select-carat rounded-none border-b-0 border-t-0 border-right-0 ';

                        if (isset($searchControls->max_price) && !$searchControls->max_price) {
                            $roundedClass .= 'lg:rounded-r-full lg:rounded-r-none ';
                        }

                        if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only' && isset($searchControls->location) && !$searchControls->location && isset($searchControls->property_type) && !$searchControls->property_type) {
                            $roundedClass .= 'lg:rounded-l-full rounded-l-none ';
                        } else {
                            $inputClass .= 'lg:border-l';
                        }
                    @endphp

                    <div class="sale_price_filter bg-white p-4 col-span-2 lg:col-span-1 {{ $salePriceHidden }} {{ $roundedClass }}">
                        @include(themeViewPath('frontend.components.selects.price-select'), [
                            'mode' => 'sale',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_sale_min_price',
                            'name' => 'sale_min_price',
                            'prices' => $searchPriceRanges['sale'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => $searchRequest->request->get('min_price'),
                            'additionalClasses' => $inputClass,
                        ])
                    </div>

                    <div class="rental_price_filter bg-white p-4 col-span-2 lg:col-span-1 {{ $rentalPriceHidden }} {{ $roundedClass }}">
                        @include(themeViewPath('frontend.components.selects.price-select'), [
                            'mode' => 'rental',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_rental_min_price',
                            'name' => 'rental_min_price',
                            'prices' => $searchPriceRanges['rental'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => $searchRequest->request->get('min_price'),
                            'additionalClasses' => $inputClass,
                        ])
                    </div>
                @endif

                @if(isset($searchControls->max_price) && $searchControls->max_price)
                    @php
                        $roundedClass = '';
                        $inputClass = '';

                        if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only' && isset($searchControls->location) && !$searchControls->location && isset($searchControls->property_type) && !$searchControls->property_type && isset($searchControls->min_price) && !$searchControls->min_price) {
                            $roundedClass .= 'lg:rounded-l-full';
                        } else {
                            $inputClass .= ' lg:border-l';
                        }
                    @endphp

                    <div class="sale_price_filter bg-white p-4 col-span-2 lg:col-span-1 lg:rounded-r-full {{ $roundedClass }} {{ $salePriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select'), [
                           'mode' => 'sale',
                           'defaultRentalFrequency' => $defaultRentalFrequency,
                           'defaultCurrency' => $defaultCurrency,
                           'id' => 'search_sale_max_price',
                           'name' => 'sale_max_price',
                           'prices' => $searchPriceRanges['sale'],
                           'initialOption' => trans('label.max_price'),
                           'currentValue' => $searchRequest->request->get('max_price'),
                           'additionalClasses' => $inputClass,
                       ])
                    </div>

                    <div class="rental_price_filter bg-white p-4 col-span-2 lg:col-span-1 lg:rounded-r-full lg:rounded-r-none {{ $rentalPriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select'), [
                           'mode' => 'rental',
                           'defaultRentalFrequency' => $defaultRentalFrequency,
                           'defaultCurrency' => $defaultCurrency,
                           'id' => 'search_rental_max_price',
                           'name' => 'rental_max_price',
                           'prices' => $searchPriceRanges['rental'],
                           'initialOption' => trans('label.max_price'),
                           'currentValue' => $searchRequest->request->get('max_price'),
                           'additionalClasses' => $inputClass,
                       ])
                    </div>
                @endif

                <div class="col-span-1 lg:col-span-1 block pt-4 lg:pt-0 pr-0 lg:pr-2">
                    <button type="button"
                            class="refine-filter-toggle-button lg:py-0 py-3 rounded-full lg:rounded-r-none block text-base lg:text-sm xl:text-base primary-bg text-white uppercase transition-all w-full h-full focus:outline-none cursor-pointer font-bold">{{ trans('label.filter') }}</button>
                </div>

                <div class="col-span-1 lg:col-span-1 pt-4 lg:pt-0">
                    <input type="submit"
                           class="lg:py-0 py-3 rounded-full block text-base lg:text-sm xl:text-base cta uppercase transition-all lg:rounded-l-none w-full h-full focus:outline-none cursor-pointer font-bold"
                           value="{{ trans('label.search') }}">
                </div>
            </div>
        </div>
        @php
            $inputClasses = '';
        @endphp

        <div class="refine-filter-toggle absolute w-full z-20 mt-3 hidden">
            <div class="relative container mx-auto px-4">
                <div class="bg-gray-100 grid lg:grid-cols-{{ $filterGridCols }} gap-0 w-full p-2 md:pb-3 rounded-lg filter">

                    @if (isset($searchControls->name) && $searchControls->name)
                        <div class="p-1 col-span-2 lg:col-span-1">
                            <div class="bg-white pr-3 lg:pr-0">
                                <input id="search_name" name="name" type="text" class="w-full block focus:outline-none bg-white text-base p-2 py-3"
                                       placeholder="{{ trans('label.property_name') }}" value="{{ $searchRequest->request->get('name') }}">
                            </div>
                        </div>
                    @endif

                    @if (isset($searchControls->reference) && $searchControls->reference)
                        <div class="p-1 col-span-2 lg:col-span-1">
                            <div class="bg-white pr-3 lg:pr-0">
                                <input id="search_reference" name="reference" type="text" class="w-full block focus:outline-none bg-white text-base p-2 py-3" placeholder="{{ trans('label.reference') }}" value="{{ $searchRequest->request->get('reference') }}">
                            </div>
                        </div>
                    @endif

                    @if(isset($searchControls->status) && $searchControls->status)
                        <div class="p-1 col-span-2 lg:col-span-1">
                            <div class="bg-white pr-3 lg:pr-0">
                                <select id="status" name="status" class="w-full block focus:outline-none bg-white text-base p-2 py-3 appearance-none select-carat">
                                    <option value="">Status</option>
                                    @foreach(\App\Models\Property::searchableTenderStatusList() as $tenderStatus)
                                        @php
                                            $selected = strtolower($searchRequest->request->get('status')) === strtolower($tenderStatus) ? 'selected' : ''
                                        @endphp
                                        <option value="{{ $tenderStatus }}" {{ $selected }}>{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    {{-- bedrooms--}}
                    @if($bedroomFilterMode !== null)
                        @if ($bedroomFilterMode === 'single')
                            <div class="p-1 col-span-2 lg:col-span-1">
                                <div class="bg-white pr-3 lg:pr-0">
                                    <select id="search_bedrooms" name="bedrooms" class="w-full block focus:outline-none bg-white text-sm p-2 py-3 appearance-none select-carat">
                                        <option value="">{{ trans('label.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedrooms)
                                            <option value="{{ $bedrooms }}" @if ($bedrooms === $searchRequest->request->get('bedrooms')) selected @endif>
                                                {{ $bedrooms }}+ {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @elseif($bedroomFilterMode === 'multi')
                            @php
                                if (strpos($searchRequest->request->get('bedrooms'), '-') !== false) {
                                    $parts = explode('-', $searchRequest->request->get('bedrooms'));
                                    $minBedsValue = isset($parts[0]) && !empty($parts[0]) ? (int) $parts[0] : null;
                                    $maxBedsValue = isset($parts[1]) && !empty($parts[1]) ? (int) $parts[1] : null;
                                } else {
                                    $minBedsValue = $searchRequest->request->get('bedrooms');
                                    $maxBedsValue = $searchRequest->request->get('bedrooms');
                                }
                            @endphp

                            <div class="p-1 col-span-2 lg:col-span-1">
                                <div class="bg-white pr-3 lg:pr-0">
                                    <select id="search_min_bedrooms" name="min_bedrooms" class="w-full block focus:outline-none bg-white text-base p-2 py-3 appearance-none select-carat">
                                        <option value="">{{ trans('label.min').' '.trans('label.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedrooms)
                                            <option value="{{ $bedrooms }}"
                                                    @if ($bedrooms === $minBedsValue) selected @endif>{{ trans('label.min') }} {{ $bedrooms }} {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="p-1 col-span-2 lg:col-span-1">
                                <div class="bg-white pr-3 lg:pr-0">
                                    <select id="search_max_bedrooms" name="max_bedrooms" class="w-full block focus:outline-none bg-white text-base p-2 py-3 appearance-none select-carat">
                                        <option value="">{{ trans('label.max').' '.trans('label.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedrooms)
                                            <option value="{{ $bedrooms }}"
                                                    @if ($bedrooms === $maxBedsValue) selected @endif>{{ trans('label.max') }} {{ $bedrooms }} {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        @endif
                    @endif

                    {{-- Bathrooms --}}

                    @if (isset($searchControls->bathrooms) && $searchControls->bathrooms)
                        <div class="p-1 col-span-2 lg:col-span-1">
                            <div class="bg-white pr-3 lg:pr-0">
                                <select id="search_bathrooms" name="bathrooms" class="w-full block focus:outline-none bg-white text-base p-2 py-3 appearance-none select-carat">
                                    <option value="">{{ trans('label.min').' '.trans('label.bathrooms') }}</option>
                                    @foreach($bathroomsRange as $bathrooms)
                                        <option value="{{ $bathrooms }}" @if ($bathrooms == $searchRequest->request->get('bathrooms')) selected @endif>
                                            {{ $bathrooms }}+ {{ trans('label.bathrooms') }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    @if($searchableFeatures->count() > 0)
                        <div class="col-span-5">
                            @include(themeViewPath('frontend.components.searchable-features'), [
                                'gridClasses' => 'grid-col-1 md:grid-cols-5 gap-2',
                                'extraClass' => 'p-2 pt-4 rounded-lg',
                            ])
                        </div>
                    @endif

                </div>
            </div>
        </div>
        @if($searchRequest->get('is_development'))
            <input type="hidden" name="is_development" value="1">
        @endif
    </form>
</section>
