<!--  Breadcrumb -->
@if (isset($navigation) && is_array($navigation))
    @php
    $i = 1;
    $count = count($navigation)
    @endphp
    <nav class="">
        <ol class="flex items-center gap-x-2 text-sm">
            @foreach($navigation as $nav)
                @php
                    $keys = array_keys($nav);
                    $values = array_values($nav);
                    $anchor = $keys[0];
                    $url = $values[0]
                @endphp

                @if ($i !== $count)
                    <li class="text-sm"><a href="{{ $url }}" class="text-sm underline duration-300 primary-text">{!! $anchor  !!}</a></li>
                @else
                    <li class="text-sm">{!! $anchor  !!}</li>
                @endif

                @if ($i !== $count)
                    <li class="text-sm">&gt;</li>
                @endif
                @php $i++ @endphp
            @endforeach
        </ol>
    </nav>
@endif
