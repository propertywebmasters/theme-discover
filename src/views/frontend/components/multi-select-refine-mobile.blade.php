<!-- multi-select-refine-mobile -->
<div class="multi-property-type-selector relative w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat">

    <span class="multi-property-type-initial-value text-sm line-clamp-1" data-initial-text="{{ trans('search.select_property_types') }}" data-selected-text="{{ trans('search.property_types_selected') }}">
        @if(isset($searchRequest) && is_array($searchRequest->get('property_type')) && count($searchRequest->get('property_type')) > 0) {{ count($searchRequest->get('property_type')) }} {{ trans('search.property_types_selected') }}  @else {{ trans('search.select_property_types') }} @endif
    </span>

    <div class="multi-property-type-options-list hidden relative md:absolute w-full top-2 left-0 click-outside-multi-select bg-white p-0 md:p-2">
        <div class="multi-property-type-options grid grid-cols-2 md:grid-cols-1 gap-y-1">

            @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_HEIRARCHICAL_CATEGORIES))
                @foreach($propertyTypes as $propertyType)
                    @php $active = isset($searchRequest) && is_array($searchRequest->get('property_type')) && in_array($propertyType->url_key, $searchRequest->get('property_type')) @endphp
                    <div class="text-sm text-black @if($active) active @endif relative"><span class="@if($active) active @endif rounded-sm inline-block w-3 h-3 border border-white text-white multi-property-type-option mr-2" data-value="{{ $propertyType->url_key }}"  style="vertical-align: middle;"></span> {{ transPropertyType($propertyType->name) }}</div>
                @endforeach
            @else
                @foreach($propertyTypeCategories as $category)
                    @php $active = isset($searchRequest) && is_array($searchRequest->get('property_type')) && in_array($category['category_slug'], $searchRequest->get('property_type')) @endphp
                    <div class="text-sm text-black @if($active) active @endif relative"><span class="@if($active) active @endif rounded-sm inline-block w-3 h-3 border border-white text-white multi-property-type-option  mr-2" data-value="{{ $category['category_slug'] }}"  style="vertical-align: middle;"></span> {{ transPropertyType($category['category']) }}</div>
                @endforeach
            @endif

        </div>
    </div>

    <div class="multi-property-type-hidden-input-wrapper"><input type="hidden" name="property_type"></div>

</div>
