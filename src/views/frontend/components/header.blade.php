@php
    use App\Models\Currency;use App\Models\SiteSetting;
    use App\Models\TenantFeature;use App\Services\Branch\Contracts\BranchServiceInterface;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;use Illuminate\Support\Str;
    $email = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);

    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')));
    $controller = get_class(request()->route()->getController());
    $method = request()->route()->getActionMethod();
    $transparentNavigation = $transparentNavigation ?? false;
    $currentCurrency = currentCurrency();
    $branches = app()->make(BranchServiceInterface::class)->all();
    $hasBranches = $branches->count() > 0;

    // work out if we should use a transparent background navigation or not
    $controller = get_class(request()->route()->getController());
    $method = request()->route()->getActionMethod();

    if (($controller === App\Http\Controllers\Frontend\HomeController::class && $method === 'index')) {
        $navClass = 'bg-transparent';
    } else if ($controller === App\Http\Controllers\Frontend\PropertyController::class && $method === 'index') {
        $navClass = 'navigation-bg';
    } else {
        $navClass = 'navigation-bg pb-2 mb-20';
    }

@endphp

@if(env('CSS_BREAKPOINT_HELPER'))
    @include('frontend.components.helpers.breakpoint-helper')
@endif

@if (hasFeature(TenantFeature::FEATURE_FIXED_GET_VALUATION_BUTTON))
    @include(themeViewPath('frontend.components.fixed-get-valuation-button'))
@else
    @include(themeViewPath('frontend.components.fixed-social'))
@endif

<nav id="main-nav" class="{{ request()->is('*thank-you') || request()->is('property*') || request()->is('development/*') ? '' : 'absolute' }} w-full top-0 right-0 {{ $navClass }}">
    <div class="container mx-auto flex justify-between flex-col lg:flex-row relative xl:px-4">
        <div class="relative z-20 px-4 md:px-0">
            <div id="logo-wrapper" class="lg:absolute top-4 left-0">
                <a id="site-logo" href="{{ localeUrl('/') }}" class="py-4 block lg:py-0 lg:inline">
                    <img class="logo lg:relative top-0 md:top-2 h-10 lg:h-12 @if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif"
                         src="{{ $logoPath }}" alt="logo" loading="lazy">
                </a>
            </div>
        </div>
        <div class="hidden lg:block mobile-menu absolute lg:relative left-0 top-full lg:top-0 pb-3 w-full lg:w-auto z-20 navigation-bg lg:bg-transparent lg:mt-4 z-40">

            <div class="container">
                <div class="hidden lg:block py-3 text-right w-auto text-sm xl:text-md nav-text border-b opacity-80">

                    <a class="inline-block text-xs nav-text pr-8" href="{{ localeUrl('/contact') }}">
                        <img src="{{ themeImage('email2.svg') }}" class="svg-inject inline-block nav-text fill-current mr-1 h-3" alt="marker" loading="lazy"> {{ $email }}
                    </a>

                    <a class="inline-block text-xs nav-text @if($hasBranches) pr-8 @endif" href="tel:{{$siteTel}}">
                        <img src="{{ themeImage('phone.svg') }}" alt="img" class="svg-inject inline-block nav-text fill-current mr-1 h-3" loading="lazy"> {{ $siteTel }}
                    </a>

                    @if ($hasBranches)
                        <a class="branch-contacts-toggle inline-block text-xs nav-text" href="javascript:">
                            <span class="text-xs transition-all inline-block whitespace-nowrap">
                                <img src="{{ themeImage('map-marker-alt.svg') }}" class="svg-inject inline-block nav-text fill-current mr-1 h-3" alt="marker" loading="lazy"> {{ trans('generic.our_branches') }} <img
                                    class="branch-drop-carat down-carat inline-block h-2 ml-2 svg-inject nav-text fill-current" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            </span>
                        </a>
                    @endif

                </div>
            </div>

            <ul class="flex flex-col lg:flex-row nav-text">
                @php $first = true @endphp
                @foreach ($navigation as $nav)
                    @php
                        $anchorLabel = getLocaleAnchorLabel($nav)
                    @endphp

                    <li class="top-level relative">
                        <a class="text-sm text-right tracking-tight px-3 py-3 @if ($first) lg:pl-0 @endif inline-block transition-all font-light"
                           href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}"
                           data-target="menu-{{ Str::slug($anchorLabel) }}">
                            {{ $anchorLabel }}
                            @if ($nav->children->count() > 0)
                                <img class="down-carat svg-inject inline-block h-2 ml-2 nav-text" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            @endif
                        </a>

                        @php $first = false @endphp
                        @if($nav->children->count() > 0)
                            <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                <ul class="primary-bg text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag dropdown-menu">
                                    @foreach ($nav->children as $child)
                                        @php
                                            $childAnchorLabel = getLocaleAnchorLabel($child)
                                        @endphp
                                        <li class="px-4 py-2 hover:cursor-pointer border-transparent">
                                            <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                               target="{{ $child->target }}">
                                                {{ $childAnchorLabel }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach

                @if(count(supportedLocales()) >= 2)
                    <li class="top-level relative lg:top-2 ml-2 lg:ml-0">
                        <a class="hover:underline" href="javascript:" data-target="menu-locale" title="Switch to {{ app()->getLocale() }}">
                            <img src="/img/flags/{{ app()->getLocale() }}.svg" class="h-6 w-6 inline-block flag-icon" alt="{{ app()->getLocale() }}" loading="lazy">
                        </a>
                        @if(count(supportedLocales()) > 1)
                            <div class="lg:absolute lg:top-6 hidden menu-locale">
                                <ul class="bg-white w-auto text-sm shadow no-drag">
                                    @foreach(supportedLocales() as $locale)
                                        @if ($locale === app()->getLocale())
                                            @continue
                                        @endif
                                        <li class="p-1 py-2 hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                            <a class="whitespace-nowrap font-light block w-full" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow"
                                               title="{{ countryName($locale) }}">
                                                <img src="/img/flags/{{ $locale }}.svg" class="svg-inject h-6 w-6 inline-block flag-icon" alt="{{ $locale }}" loading="lazy">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endif

                @if(count($tenantCurrencies) > 1)
                    <li class="top-level relative whitespace-nowrap">
                        <a class="text-sm text-right tracking-tight px-3 py-3 pr-0 inline-block transition-all whitespace-nowrap" href="javascript:" data-target="menu-currencies"
                           title="Select Currency">
                            {{ $currentCurrency->iso }} <img class="down-carat svg-inject nav-text inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                        </a>
                        <div class="nav-submenu lg:absolute lg:top-8 hidden menu-currencies bg-white">
                            <ul class="border shadow no-drag">
                                @foreach($tenantCurrencies as $currencyIso)
                                    <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                        <a class="p-2 text-sm" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                           title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                            {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif

                <li class="top-level relative whitespace-nowrap">

                    @if(user())
                        <a class="text-sm text-right tracking-tight px-3 py-3 pr-1 inline-block transition-all font-bold" href="{{ localeUrl('/account') }}">
                            <img class="h-4 mr-1 svg-inject nav-text inline-block" src="{{ themeImage('icons/person.svg') }}" alt="phone"> {{ trans('auth.my_account') }}
                        </a>
                    @else
                        <a id="auth-button" data-target="preauth-modal" class="modal-button text-sm text-right tracking-tight px-3 py-3 pr-1 inline-block transition-all font-bold"
                           href="javascript:">
                            <img class="h-4 mr-1 svg-inject nav-text fill-current inline-block" src="{{ themeImage('icons/person.svg') }}"
                                 alt="phone"> {{ trans('auth.signin_register') }}
                        </a>
                    @endif
                </li>

            </ul>
        </div>

        <div class="tham tham-e-squeeze tham-w-6 absolute top-1/2 lg:top-4 right-4 md:right-0 lg:hidden opacity-80 z-20 transform -translate-y-1/2 lg:translate-y-0" id="menu">
            <div class="tham-box">
                <div class="tham-inner"/>
            </div>
        </div>

    </div>
    <div
        class="flex items-center justify-center absolute right-10 md:right-6 top-1/2 lg:top-4 z-20 lg:hidden mobile-nav-icons opacity-80 transform -translate-y-1/2 lg:translate-y-0">
        <a href="{{ localeUrl('/contact') }}"><img class="h-4 mr-3 svg-inject nav-text fill-current" src="{{ themeImage('email2.svg') }}" alt="email"></a>
        <a href="tel:{{$siteTel}}"><img class="h-4 mr-3 svg-inject nav-text fill-current" src="{{ themeImage('phone.svg') }}" alt="phone"></a>
        @if($hasBranches)
            <a class="branch-contacts-toggle" href="javascript:">
                <img class="h-4 mr-3 svg-inject nav-text fill-current" src="{{ themeImage('map-marker-alt.svg') }}" alt="branches"></a>
        @endif
    </div>
</nav>

<header id="sticky-nav" class="navigation-bg -top-full opacity-0 transition-all fixed z-40 w-full duration-300 shadow px-4 lg:px-0">
    <div class="container mx-auto xl:px-4">
        <div class="grid grid-cols-2 items-center py-4">
            <div id="logo-wrapper">
                <a id="site-logo" href="{{ localeUrl('/') }}">
                    <img class="logo max-h-12 object-contain" src="{{ $logoPath }}" alt="logo" loading="lazy">
                </a>
            </div>

            <div>
                <div class="flex items-center justify-end lg:hidden">
                    <div class="tham tham-e-squeeze tham-w-6 opacity-80 z-20">
                        <div class="tham-box">
                            <div class="tham-inner"></div>
                        </div>
                    </div>
                </div>

                <ul class="hidden lg:flex flex-row nav-text justify-end">
                    @php $first = true @endphp
                    @foreach ($navigation as $nav)
                        @php
                            $anchorLabel = getLocaleAnchorLabel($nav)
                        @endphp

                        <li class="top-level relative">
                            <a class="text-sm text-right tracking-tight px-3 py-3 @if ($first) lg:pl-0 @endif inline-block transition-all font-light"
                               href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}"
                               data-target="menu-{{ Str::slug($anchorLabel) }}">
                                {{ $anchorLabel }}
                                @if ($nav->children->count() > 0)
                                    <img class="down-carat svg-inject inline-block h-2 ml-2 nav-text" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                                @endif
                            </a>

                            @php $first = false @endphp
                            @if($nav->children->count() > 0)
                                <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                    <ul class="primary-bg text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag dropdown-menu">
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li class="px-4 py-2 hover:cursor-pointer border-transparent">
                                                <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                   target="{{ $child->target }}">
                                                    {{ $childAnchorLabel }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </li>
                    @endforeach

                    @if(count(supportedLocales()) >= 2)
                        <li class="top-level relative lg:top-2 ml-2 lg:ml-0">
                            <a class="hover:underline" href="javascript:" data-target="menu-locale" title="Switch to {{ app()->getLocale() }}">
                                <img src="/img/flags/{{ app()->getLocale() }}.svg" class="h-6 w-6 inline-block flag-icon" alt="{{ app()->getLocale() }}" loading="lazy">
                            </a>
                            @if(count(supportedLocales()) > 1)
                                <div class="lg:absolute lg:top-6 hidden menu-locale">
                                    <ul class="bg-white w-auto text-sm shadow no-drag">
                                        @foreach(supportedLocales() as $locale)
                                            @if ($locale === app()->getLocale())
                                                @continue
                                            @endif
                                            <li class="p-1 py-2 hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                                <a class="whitespace-nowrap font-light block w-full" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow"
                                                   title="{{ countryName($locale) }}">
                                                    <img src="/img/flags/{{ $locale }}.svg" class="svg-inject h-6 w-6 inline-block flag-icon" alt="{{ $locale }}" loading="lazy">
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </li>
                    @endif

                    @if(count($tenantCurrencies) > 1)
                        <li class="top-level relative whitespace-nowrap">
                            <a class="text-sm text-right tracking-tight px-3 py-3 pr-0 inline-block transition-all whitespace-nowrap" href="javascript:"
                               data-target="menu-currencies"
                               title="Select Currency">
                                {{ $currentCurrency->iso }} <img class="down-carat svg-inject nav-text inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            </a>
                            <div class="nav-submenu lg:absolute lg:top-8 hidden menu-currencies bg-white">
                                <ul class="border shadow no-drag">
                                    @foreach($tenantCurrencies as $currencyIso)
                                        <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                            <a class="p-2 text-sm" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                               title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                                {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @endif

                    <li class="top-level relative whitespace-nowrap">

                        @if(user())
                            <a class="text-sm text-right tracking-tight px-3 py-3 pr-1 inline-block transition-all font-bold" href="{{ localeUrl('/account') }}">
                                <img class="h-4 mr-1 svg-inject nav-text inline-block" src="{{ themeImage('icons/person.svg') }}" alt="phone"> {{ trans('auth.my_account') }}
                            </a>
                        @else
                            <a id="auth-button" data-target="preauth-modal"
                               class="modal-button text-sm text-right tracking-tight px-3 py-3 pr-1 inline-block transition-all font-bold"
                               href="javascript:">
                                <img class="h-4 mr-1 svg-inject nav-text fill-current inline-block" src="{{ themeImage('icons/person.svg') }}"
                                     alt="phone"> {{ trans('auth.signin_register') }}
                            </a>
                        @endif
                    </li>

                </ul>
            </div>
        </div>
    </div>
</header>

<div class="branch-contacts-dropdown absolute top-0 left-0 w-full h-full tertiary-bg z-50 hidden transition-all" style="overflow-y: scroll;">
    <div class="container mx-auto py-4 md:py-12 pb-4 md:pb-16">

        <div class="absolute top-2 right-4 close-branches-dropdown cursor-pointer">
            <img src="{{ themeImage('close.svg') }}" alt="close">
        </div>

        <div>
            <div class="grid grid-cols-1 md:grid-cols-3 gap-8 text-white text-center md:text-left px-2 md:px-0">

                @foreach($branches as $branch)
                    <div>
                        <span class="block text-2xl py-6">{{ $branch->name }}</span>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('map-marker-alt.svg') }}"
                                                                                                              class="svg-inject primary-text fill-current inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->displayAddress() }}</div>
                        </div>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('email2.svg') }}"
                                                                                                              class="svg-inject primary-text fill-current inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->email }}</div>
                        </div>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('phone.svg') }}"
                                                                                                              class="svg-inject primary-text fill-current inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->tel }}</div>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
