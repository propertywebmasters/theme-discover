@php
    if (!isset($extraClass)) {
        $extraClass = '';
    }
@endphp

@if($reviewServices->count() > 0)
    <div @if($absolutelyPosition) class="absolute bottom-0 left-0 w-full w-full text-center review-systems py-8 z-10 {{ $extraClass }}" @endif style="background: rgba(0, 0, 0, 0.6);">
       @foreach($reviewServices as $reviewService)
            <a id="{{ $reviewService->id }}" href="{!! $reviewService->url !!}" target="_blank" class="inline-block mx-2">
                <img class="mx-auto h-10 md:h-12 lg:h-16 review-image" src="{!! assetPath($reviewService->image) !!}">
            </a>
        @endforeach
    </div>
@endif
