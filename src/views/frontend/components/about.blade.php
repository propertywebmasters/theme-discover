@php
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')))
@endphp

<section id="home-about" class="bg-whiter">
    <div class="container mx-auto text-center py-14 px-4">
        <div class="about-logo-wrapper mx-auto text-center">
            <img class="block mx-auto text-center pb-12 h-24 object-cover" src="{{ assetPath(translatableContent('home', 'about-main-image')) }}" alt="logo" loading="lazy">
        </div>
        <h2 class="header-text text-2xl lg:text-3xl xl:text-5xl tracking-tight text-primary mb-4 lg:mb-9">{!! translatableContent('home', 'about-title') !!}</h2>
        <div class="md:px-16 lg:px-32 xl:px-80">
            <p class="tracking-tight text-base leading-normal">{!! translatableContent('home', 'about-text') !!}</p>
        </div>

        <div class="text-center pt-10">
            <a class="rounded-3xl inline-block primary-bg text-white text-sm uppercase py-2 px-4 hover-lighten" href="{{ localeUrl('/about') }}">
                {{  trans('generic.read_more') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2" src="{{ themeImage('icons/right-arrow.svg') }}" loading="lazy">
            </a>
        </div>
    </div>
</section>
