@php
    $primaryGridCols = '';
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));

    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    // define default visibility for select dropdowns
    $salePriceHidden = $searchRequest->get('type') === 'rental' ? 'hidden' : '';
    $rentalPriceHidden = $searchRequest->get('type') === 'sale' ? 'hidden' : '';
    $currentCurrency = currentCurrency();

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals
@endphp

<form class="listings-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">

    <div class="p-3 md:mt-12 lg:mt-0">

        <div class="grid grid-cols-6 gap-x-1">

            <div class="col-span-4">
                {{-- @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
                    @include(themeViewPath('frontend.components.location-datalist-refine-mobile'))
                @else
                    @if(isset($searchControls->location) && $searchControls->location)
                        <input id="search_location" type="text" name="location" placeholder="{{ trans('placeholder.search_location') }}"  class="autocomplete-location rounded-md block w-full focus:outline-none h-10 bg-white text-sm border border-gray-400 py-1 px-2" autocomplete="off" data-search-results-container="mobile-search-results-container"/>
                        <input type="hidden" name="location_url" value="">
                    @endif
                @endif --}}

                @include(themeViewPath('frontend.components.location-search'), [
                    'mobile' => true,
                    'searchResultsContainer' => 'mobile-search-results-container',
                    'value' => $searchRequest->get('location'),
                    'additionalClasses' => 'rounded-md block h-10 text-sm border border-gray-400 py-1 px-2'
                ])
            </div>

            <div>
                <button class="cta block w-full text-white mx-auto text-center h-10 rounded-md py-2" type="submit">
                    <img src="{{ themeImage('icons/search.svg') }}" class="svg-inject fill-current stroke-current h-4 mx-auto" alt="{{ trans('label.search') }}" loading="lazy">
                </button>
            </div>
            <div>
                <button class="mobile-filter-popup bg-gray-400 block w-full text-white mx-auto text-center h-10 rounded-md py-2" type="button">
                    <img src="{{ themeImage('icons/filter.svg') }}" class="svg-inject fill-current stroke-current h-4 mx-auto" alt="{{ trans('label.filters') }}" loading="lazy">
                </button>
            </div>

        </div>

        <div id="autocomplete-results" class="click-outside absolute top-14 z-30 -mt-1 rounded-md hidden"  data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer" style="width: 93%">
            <ul class="mobile-search-results-container">
            </ul>
        </div>


        <div class="mobile-search-filter py-4 px-4 border-l border-r border-b border-gray-400 bg-white w-full hidden" style="margin-top: -2px;">

            <div class="grid grid-cols-1 md:grid-cols-4 gap-x-4 gap-y-4 pt-2">

                @if(isset($searchControls->name) && $searchControls->name)
                    <div class="md:col-span-2">
                        <input id="search_name" type="text" name="name" placeholder="{{ trans('label.property_name') }}"
                               value="{{ $searchRequest->get('name') }}"
                               class="autocomplete-location block w-full focus:outline-none bg-transparent text-sm border-b-1 border-gray-400 border-b border-black py-1"/>
                    </div>
                @endif

                @if(isset($searchControls->reference) && $searchControls->reference)
                    <div class="md:col-span-2">
                        <input id="search_reference" type="text" name="reference" placeholder="{{ trans('label.reference') }}"
                               value="{{ $searchRequest->get('reference') }}"
                               class="block w-full focus:outline-none bg-transparent text-sm border-b-1 border-gray-400 border-b border-black py-1"/>
                    </div>
                @endif

                @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                    <div>
                        <select id="search_type" name="type" class="w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat">
                            <option value="sale" @if ($searchRequest->get('type') === 'sale') selected @endif>{{ trans('label.sale') }}</option>
                            <option value="rental" @if ($searchRequest->get('type') === 'rental') selected @endif>{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental"
                                        @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental"
                                        @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                            @endif
                            @if($hasStudentRentals)
                                <option value="student_rental"
                                        @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                            @endif
                            @if($hasHolidayRentals)
                                <option value="holiday_rental"
                                        @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                            @endif
                        </select>
                    </div>
                @else
                    @if(isset($searchControls->tender_type))
                        @if($searchControls->tender_type === 'sale_only')
                            <input type="hidden" name="type" value="sale">
                        @elseif($searchControls->tender_type === 'rental_only')
                            <div>
                                <select id="search_type" name="type" class="w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat">
                                    <option value="rental" @if ($searchRequest->get('type') === 'rental') selected @endif>{{ trans('label.rental') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental"
                                                @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental"
                                                @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental"
                                                @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental"
                                                @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                    @endif
                                </select>
                            </div>
                        @endif
                    @endif
                @endif

                @if(isset($searchControls->property_type) && $searchControls->property_type)
                    <div>
                        @include(themeViewPath('frontend.components.multi-select-refine-mobile'))
                    </div>
                @endif

                @if($bedroomFilterMode !== null)
                    @if ($bedroomFilterMode === 'single')
                        <div>
                            <select id="search_bedrooms" name="bedrooms" class="text-sm w-full block focus:outline-none bg-transparent appearance-none select-carat">
                                <option value="">{{ trans('label.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedrooms)
                                    <option value="{{ $bedrooms }}" @if ($bedrooms === $searchRequest->get('bedrooms')) selected @endif>Min {{ $bedrooms }}+</option>
                                @endforeach
                            </select>
                        </div>
                    @elseif($bedroomFilterMode === 'multi')
                        @php
                            $parts = explode('-', $searchRequest->get('bedrooms'));
                            $minBedsValue = isset($parts[0]) && !empty($parts[0]) ? (int) $parts[0] : null;
                            $maxBedsValue = isset($parts[1]) && !empty($parts[1]) ? (int) $parts[1] : null
                        @endphp
                        <div>
                            <select id="search_min_bedrooms" name="min_bedrooms" class="text-sm w-full block focus:outline-none bg-transparent appearance-none select-carat">
                                <option value="">{{ trans('label.min').' '.trans('label.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedrooms)
                                    <option value="{{ $bedrooms }}" @if ($bedrooms === $minBedsValue) selected @endif>{{ trans('label.min') }} {{ $bedrooms }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <select id="search_max_bedrooms" name="max_bedrooms" class="text-sm w-full block focus:outline-none bg-transparent appearance-none select-carat">
                                <option value="">{{ trans('label.max').' '.trans('label.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedrooms)
                                    <option value="{{ $bedrooms }}" @if ($bedrooms === $maxBedsValue) selected @endif>{{ trans('label.max') }} {{ $bedrooms }}+</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                @endif

                @if(isset($searchControls->bathrooms) && $searchControls->bathrooms)
                    <div>
                        <select id="search_bathrooms" name="bathrooms" class="text-sm block text-black w-full focus:outline-none bg-transparent appearance-none select-carat">
                            <option value="">{{ trans('placeholder.any_bathrooms') }}</option>
                            @foreach(range(1, 5) as $bathrooms)
                                <option value="{{ $bathrooms }}" @if ($bathrooms == $searchRequest->request->get('bathrooms')) selected @endif>Min {{ $bathrooms }}+</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if(isset($searchControls->min_price) && $searchControls->min_price)
                    <div class="sale_price_filter {{ $salePriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select-refine'), [
                            'mode' => 'sale',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_sale_min_price',
                            'name' => 'sale_min_price',
                            'prices' => $searchPriceRanges['sale'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => $searchRequest->request->get('min_price'),
                        ])
                    </div>
                @endif

                @if(isset($searchControls->max_price) && $searchControls->max_price)
                    <div class="sale_price_filter {{ $salePriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select-refine'), [
                            'mode' => 'sale',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_sale_max_price',
                            'name' => 'sale_max_price',
                            'prices' => $searchPriceRanges['sale'],
                            'initialOption' => trans('label.max_price'),
                            'currentValue' => $searchRequest->request->get('max_price'),
                        ])
                    </div>
                @endif

                @if(isset($searchControls->min_price) && $searchControls->min_price)
                    <div class="rental_price_filter {{ $rentalPriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select-refine'), [
                            'mode' => 'rental',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_rental_min_price',
                            'name' => 'rental_min_price',
                            'prices' => $searchPriceRanges['rental'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => $searchRequest->request->get('min_price'),
                        ])
                    </div>
                @endif

                @if(isset($searchControls->max_price) && $searchControls->max_price)
                    <div class="rental_price_filter {{ $rentalPriceHidden }}">
                        @include(themeViewPath('frontend.components.selects.price-select-refine'), [
                            'mode' => 'rental',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'search_rental_max_price',
                            'name' => 'rental_max_price',
                            'prices' => $searchPriceRanges['rental'],
                            'initialOption' => trans('label.max_price'),
                            'currentValue' => $searchRequest->request->get('max_price'),
                        ])
                    </div>
                @endif

                @if(isset($searchControls->status) && $searchControls->status)
                    <div class="md:col-span-2">
                        <select id="status" name="status" class="w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat">
                            <option value="">Status</option>
                            @foreach(\App\Models\Property::searchableTenderStatusList() as $tenderStatus)
                                @php
                                    $selected = strtolower($searchRequest->get('status')) === strtolower($tenderStatus) ? 'selected' : ''
                                @endphp
                                <option value="{{ $tenderStatus }}" {{ $selected }}>{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="hidden sm:block"></div>

                @if($searchableFeatures->count() > 0)
                    <div class="md:col-span-4">
                        @include(themeViewPath('frontend.components.searchable-features'), [])
                    </div>
                @endif

                <div class="md:col-span-4">
                    <div>
                        <span class="reset-search-filter-button block text-md text-white font-bold uppercase w-full h-full focus:outline-none cursor-pointer cta hover-lighten p-1 py-2 text-center mb-2" data-container=".mobile-search-filter">{{ trans('generic.reset') }}</span>
                    </div>

                    <div>
                        <input type="submit" class="block text-md text-white font-bold uppercase w-full h-full focus:outline-none cursor-pointer cta hover-lighten p-1 py-2" value="{{ trans('label.search') }} &rarr;"/>
                    </div>
                </div>
            </div>


        </div>

    </div>
    @if($searchRequest->get('is_development'))
        <input type="hidden" name="is_development" value="1">
    @endif
</form>
