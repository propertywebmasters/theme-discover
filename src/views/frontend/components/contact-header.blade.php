@php
    use App\Models\SiteSetting;use App\Services\Branch\Contracts\BranchServiceInterface;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $branches = app()->make(BranchServiceInterface::class)->all();
    $hasBranches = $branches->count() > 0;
    $whatsappTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_WHATSAPP_NUMBER['key']);
    $transparentNavigation = true;
    $transparentNavigation = $transparentNavigation ?? false
@endphp

<style>

</style>

<header
    class="absolute top-0 right-0 relative @if ($transparentNavigation) bg-transparent @else tertiary-bg @endif pt-0 pb-0 lg:pt-2 lg:pb-2 px-2 @if (!$hasBranches) hidden lg:block @endif"
    style="background-color: transparent;">
    <div class="container mx-auto text-center text-right px-4">
        <ul class="hidden lg:flex justify-between md:justify-end">
            <li class="hidden lg:inline-block text-center md:text-left">
                <a class="text-white text-sm flex" href="{{ localeUrl('/contact') }}">
                    <img class="svg-inject fill-current primary-text mr-2 h-3 mt-0.5" src="{{ themeImage('email.svg') }}" alt="email">
                    <span class="text-xs transition-all inline-block whitespace-nowrap">{{ $email }}</span>
                </a>
            </li>
            <li class="hidden lg:inline-block text-center md:text-left ml-5">
                <a class="text-white text-sm flex" href="tel:{{$siteTel}}">
                    <img class="svg-inject fill-current primary-text mr-2 h-3 mt-0.5" src="{{ themeImage('phone-alt.svg') }}" alt="phone">
                    <span class="text-xs transition-all inline-block whitespace-nowrap">{{ $siteTel }}</span>
                </a>
            </li>

            @if($whatsappTel !== null && $whatsappTel !== '')
                <li class="hidden lg:inline-block text-center md:text-left ml-5">
                    <a class="text-white text-sm flex" href="https://api.whatsapp.com/send?phone={{ $whatsappTel }}" target="_BLANK">
                        <img class="svg-inject fill-current stroke-current primary-text mr-2 h-3 mt-0.5" src="{{ themeImage('whatsapp.svg') }}" alt="whatsapp">
                        <span class="text-xs transition-all inline-block whitespace-nowrap">{{ $whatsappTel }}</span>
                    </a>
                </li>
            @endif

            @if ($branches->count() > 0)
                <li class="branch-contacts-toggle text-center md:text-left ml-5">
                    <a class="text-white text-sm flex" href="javascript:">
                        <img class="svg-inject fill-current primary-text mr-2 h-3 mt-0.5" src="{{ themeImage('map-marker-alt.svg') }}" alt="email">
                        <span class="text-xs transition-all inline-block whitespace-nowrap">
                            {{ trans('generic.our_branches') }} <img class="branch-drop-carat down-carat inline-block h-2 ml-2 svg-inject primary-text fill-current"
                                                                     src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                        </span>
                    </a>
                </li>
            @endif
        </ul>

        <div class="lg:hidden block w-full mx-auto text-center pt-2">
            <div class="grid grid-cols-3 md:grid-cols-5">
                <div class="col-span-1 md:col-span-2">&nbsp;</div>
                <div class="col-span-1 text-center">
                    <a class="branch-contacts-toggle text-white text-sm flex" href="javascript:">
                        <img class="svg-inject fill-current primary-text h-3 mr-2" src="{{ themeImage('map-marker-alt.svg') }}" alt="email">
                        <span class="text-xs transition-all inline-block whitespace-nowrap">
                            {{ trans('generic.our_branches') }} <img class="branch-drop-carat down-carat inline-block h-2 ml-2 svg-inject primary-text fill-current"
                                                                     src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                        </span>
                    </a>
                </div>
                <div class="col-span-1 md:col-span-2">&nbsp;</div>
            </div>
        </div>
    </div>

    <div class="branch-contacts-dropdown absolute top-8 left-0 w-full tertiary-bg hidden z-100" style="border-top: 1px solid var(--ap-nav-bg);">
        <div class="container mx-auto py-4 md:py-12 pb-4 md:pb-16">
            <div>
                <div class="grid grid-cols-1 md:grid-cols-3 gap-8 text-white text-center md:text-left px-2 md:px-0">

                    @foreach($branches as $branch)
                        <div>
                            <span class="block text-2xl py-6">{{ $branch->name }}</span>

                            <div class="grid grid-cols-12 pb-2 md:pb-4">
                                <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('map-marker-alt.svg') }}"
                                                                                                                  class="svg-inject primary-text fill-current inline-block"
                                                                                                                  alt="phone" loading="lazy"></div>
                                <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->displayAddress() }}</div>
                            </div>

                            <div class="grid grid-cols-12 pb-2 md:pb-4">
                                <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('email2.svg') }}"
                                                                                                                  class="svg-inject primary-text fill-current inline-block"
                                                                                                                  alt="phone" loading="lazy"></div>
                                <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->email}}</div>
                            </div>

                            <div class="grid grid-cols-12 pb-2 md:pb-4">
                                <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('phone.svg') }}"
                                                                                                                  class="svg-inject primary-text fill-current inline-block"
                                                                                                                  alt="phone" loading="lazy"></div>
                                <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->tel }}</div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</header>
