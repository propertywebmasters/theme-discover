@if($testimonials->count() > 0)
    @php
    $testimonial = $testimonials->first()
    @endphp

    @php
    $testimonialCount = $testimonials->count() >= 3 ? 3 : 1
    @endphp

    <section id="testimonials" class="bg-whiter md:py-16 pt-14 pb-0 xl:px-80 lg:px-40 px-4" data-slides-per-view="{{ $testimonialCount }}">
        <div class="container mx-auto px-4">
            <h3 class="header-text py-2 text-2xl md:text-3xl text-center tracking-tight text-primary">{{ trans('header.what_our_customers_think') }}</h3>
        </div>
        <!-- Swiper -->
        <div class="testimonial-slider container mx-auto overflow-hidden">
            <div class="swiper-wrapper">
                @foreach ($testimonials as $testimonial)
                    @include(themeViewPath('frontend.components.cards.testimonial'), ['testimonial' => $testimonial])
                @endforeach
            </div>
        </div>
    </section>
@endif
