@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';
    $roundedClass = 'rounded-md';
    $borderClass = '';
    if ($searchControls->tender_type === 'sale_only') {
        $roundedClass = 'rounded-t-lg lg:rounded-full lg:rounded-r-none';
    }

    if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
        $borderClass .= 'lg:border-l ';
    }

    if (isset($searchControls->property_type) && $searchControls->property_type) {
        $borderClass .= 'lg:border-r ';
    }
@endphp

<div class="w-full location-datalist-container">

    {{-- <input class="placeholder-black location-datalist-input w-full rounded-md block w-full focus:outline-none h-10 bg-white text-sm border border-gray-400 py-1 px-2"  autocomplete="off" role="combobox" list="" name="location" placeholder="{{ trans('placeholder.search_location') }}" value="{{ $locationValue }}"> --}}

    @include(themeViewPath('frontend.components.location-search-input'), ['datalist' => true, 'value' => $locationValue, 'additionalClasses' => 'w-full rounded-md block w-full focus:outline-none h-10 bg-white text-sm border border-gray-400 py-1 px-2'])

    <datalist id="browsers" role="listbox" class="absolute w-full text-xs top-14 left-0 " style="margin-top: 1px; z-index: 999;">
        <div class="text-left text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-50 bg-gray-50 rounded-b-lg">
            @php $i = 1 @endphp
            @foreach($dataList as $text => $value)
                <option class="text-xs bg-gray-50 text-black cursor-pointer hover:bg-white py-2 px-4 @if($i === 1) pt-4 @endif" data-value="{{ $value }}">{{ $text }}</option>
                @php $i++ @endphp
            @endforeach
        </div>
    </datalist>

    <input type="hidden" name="location_url">
</div>
