@php
    use App\Models\TenantFeature;$propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }
@endphp

@if($propertyDescription !== null)
    <div class="lg:mb-16">
        <h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ trans('header.property_description') }}</h3>
        <p class="text-base leading-normal tracking-tight text-browngrey">{!! optional($propertyDescription)->long !!}</p>

        @if(hasFeature(TenantFeature::FEATURE_PDF_GENERATION))
            @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
                <a data-target="brochure-request-modal"
                   class="download-pdf mt-8 text-base text-center tracking-wide font-bold header-text uppercase rounded-3xl border border-solid primary-border primary-text inline-block py-3 px-8 transition-all modal-button"
                   href="javascript:">{{ trans('generic.download_pdf') }}</a>
            @else
                <a class="download-pdf mt-4 sm:mt-8 text-base text-center tracking-wide font-bold header-text uppercase rounded-3xl border border-solid primary-border primary-text inline-block py-3 px-8 transition-all"
                   href="{{ $property->pdf_url }}" target="_BLANK">{{ trans('generic.download_pdf') }}</a>
            @endif
        @endif
    </div>
@endif

<div class="lg:mb-16">
    <strong>{{ trans('label.property_type') }}:</strong> {{ trans('property_types.'.strtolower($property->propertyType->name)) }}<br>
    @if($property->tenure !== null)
        <strong>{{ trans('generic.tenure') }}</strong>: {{ ucwords($property->tenure) }}<br>
    @endif
</div>

