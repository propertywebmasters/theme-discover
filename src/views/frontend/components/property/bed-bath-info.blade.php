@php
    $extraClasses = $extraClasses ?? '';
    $showSizing = $showSizing ?? false;
@endphp

<div class="flex lg:mt-6 {{ $extraClasses }}">
    @if ($property->isHouse())
        @if($property->bedrooms != 0)
            <div class="flex mr-3">
                <img src="{{ themeImage('bed.svg') }}" alt="bed" loading="lazy" class="primary-text fill-current svg-inject h-4 mt-1">
                <span class="inline-block ml-2 text-grey text-base">{{ $property->bedrooms }}</span>
            </div>
        @endif
        @if($property->bathrooms != 0)
            <div class="flex mr-3">
                <img src="{{ themeImage('bath.svg') }}" alt="bath" loading="lazy" class=" primary-text fill-current svg-inject h-4 mt-1">
                <span class="inline-block ml-2 text-grey text-base">{{ str_replace('.0', '', $property->bathrooms) }}</span>
            </div>
        @endif
    @endif

    @if($showSizing)
        @if($property->internal_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex mr-3">
                <img src="{{ themeImage('internal-size.svg') }}" alt="resize" loading="lazy" class="primary-text fill-current stroke-current svg-inject h-5 mt-0.5">
                <span class="inline-block ml-2 text-grey text-base">{{ $property->internal_size }} {!! $unit !!}</span>
            </div>
        @endif
        @if($property->land_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex">
                <img src="{{ themeImage('land-size.svg') }}" alt="resize" loading="lazy" class="primary-text fill-current stroke-current svg-inject h-5 mt-0.5">
                <span class="inline-block ml-2 text-grey text-base">{{ $property->land_size }} {!! $unit !!}</span>
            </div>
        @endif
    @endif



</div>
