@php
    use App\Models\TenantFeature;
    $classes = 'w-full whitespace-nowrap text-sm lg:text-base text-center tracking-wide font-bold uppercase rounded-3xl block transition-all py-3 cta';
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
    $hasValuationFeature = hasFeature(TenantFeature::FEATURE_VALUATION_SYSTEM);
    $buttonCount = $searchButtons->count();
    $mdGridCols = $buttonCount;
    $lgGridCols = 4;

    if ($buttonCount > 2) {
        $mdGridCols = 2;
    }

    if ($buttonCount < $lgGridCols) {
        $lgGridCols = $buttonCount;
    }

    $buttonCount = $searchButtons->count();
    $isOdd = $buttonCount % 2 == 1;
@endphp

<div class="mx-auto w-4/5 lg:w-1/2 pt-6 pb-2">
    <div class="container">
        <div class="mx-auto">
            @if($searchButtons->count() > 0)
                @php $i = 1; @endphp
            <div class="grid grid-cols-{{ $mdGridCols }} lg:grid-cols-{{ $lgGridCols }} gap-x-4 gap-y-4 items-center lg:items-start">
                @foreach($searchButtons as $button)
                    <div class="@if($i == $buttonCount && $isOdd) col-span-2 md:col-span-1 @endif"> <!-- {{ $i }} === {{ $buttonCount }} -->
                        <a class="{{ $classes }} " href="{{ localeUrl($button->url) }}">{{ $button->text }}</a>
                    </div>
                    @php $i++ @endphp
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
