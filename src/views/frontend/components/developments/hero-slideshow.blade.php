@php
    // if the slide is empty then we will inherit from theme default
    use App\Models\Slide;use App\Services\Content\ContentBlockService;use App\Support\ThemeManager;$imagePath = ThemeManager::backgroundCSS('developments.hero', tenantManager()->getTenant(), true);
    $slide = new Slide;
    $slide->image = $imagePath;
    $slide->title = optional(app()->make(ContentBlockService::class)->getByIdentifier('developments', 'hero-title', tenantManager()->getTenant(), app()->getLocale()))->content;
    $slides = collect()->push($slide)
@endphp

<div class="relative">
    <section id="home-hero" class="relative">
        {{-- loop this --}}
        <div id="home-slides" data-speed="12000">
            @php $i = 1 @endphp
            @foreach ($slides as $slide)
                @php
                    $displayClass = $i === 1 ? '' : ' hidden';
                    $active = $i === 1 ? 'active' : '';
                    $tag = $i === 1 ? 'h1' : 'h2'
                @endphp
                <div class="slide slide-{{$i}} {{ $active }} {{ $displayClass }}" data-slide-number="{{ $i }}">
                    <div class="relative center-cover-bg bg-lazy-load h-158 py-32" data-style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ assetPath($slide->image) }}')">
                        <div class="absolute top-16 md:top-44 lg:top-56 leading-10 w-full block line-clamp-3 lg:line-clamp-2">
{{--                            <{{$tag}} class="header-text block h-full text-white text-3xl mx-auto w-4/5 xl:w-3/5 md:text-3xl lg:text-5xl text-center drop-shadow-lg">{!! $slide->title !!}</{{$tag}}>--}}
                            <{{$tag}} class="header-text block h-full text-white text-3xl mx-auto w-4/5 xl:w-3/5 md:text-3xl lg:text-5xl text-center drop-shadow-lg">{{ $slide->title }}</{{$tag}}>
                        </div>
                    </div>
                </div>
            @php $i++ @endphp
            @endforeach
        </div>

    @if($slides->count() > 1)
        <div id="home-slides-controls" class="absolute bottom-3 right-0 w-full text-center">
            @php $i = 1 @endphp
            @foreach ($slides as $slide)
                @php
                    $bgClass = $i === 1 ? 'primary-bg' : 'secondary-bg';
                    $active = $i === 1 ? 'active' : ''
                @endphp
                <span class="{{ $bgClass }} {{ $active }} inline-block rounded-full h-4 w-4 mr-1 border border-white cursor-pointer" data-slide-number="{{ $i }}">&nbsp;</span>
                @php $i++ @endphp
            @endforeach
        </div>
    @endif

    @include(themeViewPath('frontend.components.home-development-search'), ['absolutelyPosition' => true])
    </section>
</div>
