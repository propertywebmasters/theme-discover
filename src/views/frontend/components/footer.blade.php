@php
    use App\Models\SiteSetting;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;$useSettingsCache = config('platform-cache.site_settings_enabled');
    $useFooterCache = config('platform-cache.footers_enabled');

    $settingService = app()->make(SiteSettingServiceInterface::class);

    $siteLogo = assetPath($settingService->retrieve(SiteSetting::SETTING_LOGO['key'], $useSettingsCache));
    $footerLogo = assetPath($settingService->retrieve(SiteSetting::SETTING_FOOTER_LOGO['key'], $useSettingsCache));
    $siteAddress = $settingService->retrieve(SiteSetting::SETTING_SITE_ADDRESS['key'], $useSettingsCache);
    $siteEmail = $settingService->retrieve(SiteSetting::SETTING_SITE_EMAIL['key'], $useSettingsCache);
    $siteTel = $settingService->retrieve(SiteSetting::SETTING_SITE_TEL['key'], $useSettingsCache);
    $logoPath = !empty($footerLogo) ? $footerLogo : $siteLogo
@endphp

@if(hasFeature(\App\Models\TenantFeature::FEATURE_MAILING_LIST))
    <section id="newsletter-signup" class="tertiary-bg">
        <div class="container mx-auto py-12">
            @include(themeViewPath('frontend.components.newsletter-signup'))
        </div>
    </section>

@endif

<footer class="footer-section md:pt-8 pb-5 xl:px-4">

    <div class="container mx-auto pt-10">
        <div class="text-center mx-auto md:text-left ml-0 md:ml-6">
            <img src="{{ $logoPath }}" alt="logo" class="mb-8 text-center mx-auto md:hidden w-1/2 md:w-2">
        </div>
        <div class="text-center md:text-left grid md:grid-cols-3 lg:grid-cols-{{ $footerBlocks->count() }} gap-4 pb-0 md:pb-10">
            @foreach($footerBlocks as $block)
                <div class="border-b-2 md:border-b-0 pb-9 md:pb-0 mb-2 md:mb-0">
                    <h3 class="text-lg tracking-tight font-bold">{!! localeStringFromCollection($block, null, $useFooterCache) !!}</h3>
                    <ul class="mt-3">
                        @foreach($block->navigation as $link)
                            <li>
                                <a class="text-base leading-normal tracking-tight" href="{{ localeUrl($link->url) }}" target="{{ $link->target }}">
                                    {!! localeStringFromCollection($link, null, $useFooterCache) !!}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
    @include('frontend.components.accreditations')
    @include(themeViewPath('frontend.components.social-footer'))
</footer>
