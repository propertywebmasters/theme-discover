<section id="home-features" class="py-16">
    <div class="container mx-auto px-4">
        <h3 class="text-3xl text-center header-text">{!! translatableContent('home', 'feature-header') !!}</h3>
        <div class="grid lg:grid-cols-4 md:grid-cols-2 xl:gap-20 lg:gap-10 sm:gap-8">
            <!-- Single -->
            <div class="text-center mt-12">
                <img class="svg-inject primary-text inline-block mb-5 h-12" src="{{ themeImage('about/star.svg') }}" alt="star" loading="lazy">
                <h3 class="text-2xl text-center header-text mb-3">{!! translatableContent('home', 'feature-block-1-title') !!}</h3>
                <p class="text-sm leading-normal text-center tracking-tight font-light">
                    {!! translatableContent('home', 'feature-block-1-text') !!}
                </p>
            </div>

            <!-- Single -->
            <div class="text-center mt-12">
                <img class="svg-inject primary-text inline-block mb-5 h-12" src="{{ themeImage('about/star.svg') }}" alt="star" loading="lazy">
                <h3 class="text-2xl text-center header-text mb-3">{!! translatableContent('home', 'feature-block-2-title') !!}</h3>
                <p class="text-sm leading-normal text-center tracking-tight font-light">
                    {!! translatableContent('home', 'feature-block-2-text') !!}
                </p>
            </div>
            <!-- Single -->
            <div class="text-center mt-12">
                <img class="svg-inject primary-text inline-block mb-5 h-12" src="{{ themeImage('about/star.svg') }}" alt="star" loading="lazy">
                <h3 class="text-2xl  text-center header-text mb-3">
                    {!! translatableContent('home', 'feature-block-3-title') !!}
                </h3>
                <p class="text-sm leading-normal text-center tracking-tight font-light">
                    {!! translatableContent('home', 'feature-block-3-text') !!}
                </p>
            </div>
            <!-- Single -->
            <div class="text-center mt-12">
                <img class="svg-inject primary-text inline-block mb-5 h-12" src="{{ themeImage('about/star.svg') }}" alt="star" loading="lazy">
                <h3 class="text-2xl  text-center header-text mb-3">
                    {!! translatableContent('home', 'feature-block-4-title') !!}
                </h3>
                <p class="text-sm leading-normal text-center tracking-tight font-light">
                    {!! translatableContent('home', 'feature-block-4-text') !!}
                </p>
            </div>
        </div>
    </div>
</section>
