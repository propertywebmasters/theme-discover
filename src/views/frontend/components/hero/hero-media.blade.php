<div class="relative">
    <section id="home-hero" class="relative">

        @if ($video)
            @if ($video->fallback_image_enabled)
                <div class="hidden h-158 hero-fallback-image-container">
                    @include(themeViewPath('frontend.components.hero.hero-fallback-image'))
                </div>

                <div class="hidden hero-video-container">
                    @include(themeViewPath('frontend.components.hero.hero-video'))
                </div>
            @else
                @include(themeViewPath('frontend.components.hero.hero-video'))
            @endif
        @else
            @include(themeViewPath('frontend.components.hero.hero-slideshow'))
        @endif

        @include(themeViewPath('frontend.components.hero-reviews'), ['absolutelyPosition' => true])

    </section>
</div>
