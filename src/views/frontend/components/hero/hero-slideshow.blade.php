@php
    use App\Models\Slide;
    use App\Services\Content\ContentBlockService;
    use App\Support\ThemeManager;
    use App\Models\TenantFeature;

    // if the slide is empty then we will inherit from theme default
    if($slides->count() === 0) {
        $imagePath = ThemeManager::backgroundCSS('home.hero', tenantManager()->getTenant(), true);

        $slide = new Slide;
        $slide->image = $imagePath;
        $slide->title = optional(app()->make(ContentBlockService::class)->getByIdentifier('home', 'hero-slogan', tenantManager()->getTenant(), app()->getLocale()))->content;
        // $slide->title = 'Here is some really really long text that should span to two lines';
        $slides = collect()->push($slide);
    }

    $class = 'absolute leading-10 w-full block hero-title';

    $slideSubtitlesEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SUBTITLES);
@endphp

<div class="relative">
    <section id="home-hero" class="relative">
        {{-- loop this --}}
        <div id="home-slides" data-speed="12000">
            @php $i = 1 @endphp
            @foreach ($slides as $slide)
                @php
                    $displayClass = $i === 1 ? '' : ' hidden';
                    $active = $i === 1 ? 'active' : '';
                    $tag = $i === 1 ? 'h1' : 'h2'
                @endphp
                <div class="slide slide-{{$i}} {{ $active }} {{ $displayClass }}" data-slide-number="{{ $i }}">
                    <div class="relative center-cover-bg bg-lazy-load min-h-screen py-32" data-style="background: url('{{ assetPath($slide->image) }}');">
                        <div class="absolute h-full w-full top-0 left-0 slide-overlay" style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));"></div>
                        <div class="{{ $class }}">
                            <{{$tag}} class="line-clamp-2 header-text block h-full text-white text-2xl sm:text-3xl w-auto mx-auto md:text-5xl text-center w-4/5 lg:w-1/2 {{ $slide->subtitle && $slideSubtitlesEnabled ? '' : 'border-b' }} pb-4">{!! $slide->title  !!}</{{$tag}}>

                            @if ($slideSubtitlesEnabled && $slide->subtitle)
                                <h3 class="header-text block h-full text-white text-2xl w-auto mx-auto md:text-4xl text-center w-4/5 lg:w-1/2 border-b pb-4">{{ $slide->subtitle }}</h3>
                            @endif

                            @include(themeViewPath('frontend.components.home-search'), ['absolutelyPosition' => false])
                    </div>
                </div>
        </div>
    @php $i++ @endphp
    @endforeach
</div>

@if($slides->count() > 1)
    <div id="home-slides-controls" class="absolute bottom-3 right-0 w-full text-center z-30">
        @php $i = 1 @endphp
        @foreach ($slides as $slide)
            @php
                $bgClass = $i === 1 ? 'primary-bg' : 'secondary-bg';
                $active = $i === 1 ? 'active' : ''
            @endphp
            <span class="{{ $bgClass }} {{ $active }} inline-block rounded-full h-4 w-4 mr-1 border border-white cursor-pointer" data-slide-number="{{ $i }}">&nbsp;</span>
            @php $i++ @endphp
        @endforeach
    </div>
    @endif

    </section>
    </div>
