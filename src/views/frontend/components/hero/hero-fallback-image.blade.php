<div class="relative min-h-screen overflow-hidden">
    <div class="absolute video-overlay w-full h-full inset bg-black z-2" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}" @else style="opacity: 0;" @endif></div>

    <div class="absolute bottom-2/3 lg:bottom-1/2 pb-4 w-full z-20">
        <h1 class="block text-white pb-6 text-2xl mx-auto w-4/5 xl:w-3/5 md:text-5xl text-center drop-shadow-lg header-text">{!! $video->title !!}</h1>
        <div class="absolute top-1/2 block w-full">
        @include(themeViewPath('frontend.components.home-search'), ['absolutelyPosition' => false])
        </div>
    </div>

    <div class="absolute z-1 w-full h-full">
        <div style="background-repeat: no-repeat; background-image: url('{{ assetPath($video->fallback_image) }}')" class="center-cover-bg h-full w-full"></div>
    </div>
</div>
