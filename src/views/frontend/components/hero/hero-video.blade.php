<div class="relative min-h-screen overflow-hidden">
    <div class="absolute video-overlay w-full h-full inset bg-black z-10" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}" @else style="opacity: 0;" @endif></div>

    <div class="absolute top-1/3 lg:bottom-1/2 pb-4 w-full z-20">
        <h1 class="block text-white pb-6 mx-auto w-4/5 xl:w-3/5 text-4xl md:text-5xl text-center drop-shadow-lg header-text">{!! $video->title !!}</h1>
        @include(themeViewPath('frontend.components.home-search'), ['absolutelyPosition' => false])
    </div>

    <div class="absolute z-1 w-full h-full">

        @if($video->provider === 'internal')
            <video  class="w-full h-full block transform scale-600 sm:scale-400 md:scale-200 absolute" style="filter: blur(20px); " muted autoplay>
                <source src="{{ $video->getEmbedUrl() }}" type="video/mp4" />
                Your browser does not support the video tag.
            </video>
            <video  class="w-full h-full block absolute" muted autoplay>
                <source src="{{ $video->getEmbedUrl() }}" type="video/mp4" />
                Your browser does not support the video tag.
            </video>
        @else
            <iframe loading="lazy" class="w-full h-full block transform scale-600 sm:scale-400 md:scale-200" src="{{ $video->getEmbedUrl() }}" title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endif

    </div>
</div>
