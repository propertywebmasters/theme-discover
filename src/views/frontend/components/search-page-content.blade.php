@if (isset($searchContent) && $searchContent !== null && !request()->has('page'))
    <section class="search-content bg-whiter">
        <div class="container mx-auto py-12 px-4">
            <div class="border-b-2 border-gray-200 mb-5">
                <h2 class="text-2xl md:text-4xl pb-5 header-text">{{ $searchContent->title }}</h2>
            </div>
            <p class="">{!! $searchContent->content !!}</p>
        </div>
    </section>
@endif
