@php
$title = $customHeader ?? trans('header.latest_news')
@endphp

@if ($articles!== null && $articles->count() > 0)
    @php
        $i = 0
    @endphp
    <section id="latest-news" class="mb-12 md:mb-20">
        <div class="container mx-auto bg-white px-4 sm:px-8 xl:px-4">
            <div class="flex justify-between pb-7 md:pt-12 pt-7">
                <div class="w-full mx-auto text-center">
                    <h3 class="block header-text text-xl text-center md:text-3xl leading-tight tracking-tight text-primary mx-auto text-center">{{ $title }}</h3>
                </div>
            </div>
            <div class="grid md:grid-cols-2 xl:grid-cols-4 gap-4">
                @php $i = 1 @endphp
                @foreach($articles as $article)
                    @include(themeViewPath('frontend.components.cards.article'), ['article' => $article,])
                    @php $i++ @endphp
                @endforeach
            </div>

            <div class="text-center pt-10">
                <a class="rounded-3xl inline-block primary-bg text-white text-sm uppercase py-2 px-4 hover-lighten" href="{{ localeUrl('/news') }}">
                    {{  trans('header.view_archive') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2" src="{{ themeImage('icons/right-arrow.svg') }}" loading="lazy">
                </a>
            </div>

        </div>
    </section>
@endif
