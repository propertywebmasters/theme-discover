@if ($branches->count() > 0)
    @foreach($branches as $branch)
        @include(themeViewPath('frontend.components.cards.branch'))
    @endforeach
@endif
