@php
    $i = 0;
    $saleProperties = $properties->where('tender_type', 'sale');
    $rentalProperties = $properties->where('tender_type', 'rental');

    $isMultiMode = $saleProperties->count() > 0 && $rentalProperties->count() > 0;
@endphp

@if ($properties->count() > 0)
    @php
        $header = trans('header.similar_properties');
        if (isset($customHeader)) {
            $header = $customHeader;
        }
    @endphp

    <section id="featured-properties" class="mb-5 lg:mb-24 sm:px-4">
        <div class="container mx-auto bg-white px-4 md:px-0">
            <div class="flex flex-col sm:flex-row justify-between mt-8 mb-6 md:my-12 items-center relative">
                <div class="sm:w-full">
                    <h3 class="header-text text-xl md:text-3xl leading-normal md:text-center tracking-tight text-primary">{{ $customHeader }}</h3>
                </div>
                @if($isMultiMode)
                    <div class="flex sm:absolute inset-y-1/2 transform -translate-y-1/2 right-0 h-full items-center">
                        <a href="javascript:" class="tab-switcher text-base text-right primary-text" data-on-classes="primary-text" data-show="tab-featured-sale" data-hide="tab-featured-rental">
                            {{ \Illuminate\Support\Str::plural(trans('label.sale')) }}
                        </a>    
                        <span class="inline-block px-2">|</span>
                        <a href="javascript:" class="tab-switcher text-base" data-on-classes="primary-text" data-show="tab-featured-rental" data-hide="tab-featured-sale">
                            {{ \Illuminate\Support\Str::plural(trans('label.rental')) }}
                        </a>
                    </div>
                @endif
            </div>

            @if($isMultiMode)
                <div class="tab-featured-sale grid md:grid-cols-2 xl:grid-cols-4 gap-4">
                    @php $i = 1 @endphp
                    @foreach($saleProperties as $property)
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>

                <div class="tab-featured-rental hidden grid md:grid-cols-2 xl:grid-cols-4 gap-4 hidden">
                    @php $i = 1 @endphp
                    @foreach($rentalProperties as $property)
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>
            @else
                <div class="grid md:grid-cols-2 xl:grid-cols-4 gap-4">
                    @php $i = 1 @endphp
                    @foreach($properties as $property)
                        @if($i > 12) @continue @endif
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>
            @endif

        </div>
    </section>


@endif
