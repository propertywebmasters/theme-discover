@php
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;

    $filterGridCols = 5;

    $primaryGridCols = 9;
    // $secondaryGridCols = 9;

    if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
        --$primaryGridCols;
    }

    if (isset($searchControls->location) && !$searchControls->location) {
        $primaryGridCols -= 3;
    }

    if (isset($searchControls->property_type) && !$searchControls->property_type) {
        --$primaryGridCols;
    }

    if (isset($searchControls->max_price) && !$searchControls->max_price) {
        --$primaryGridCols;
    }

    if (isset($searchControls->min_price) && !$searchControls->min_price) {
        --$primaryGridCols;
    }

    if (isset($searchControls->bedrooms) && $searchControls->bedrooms) {
        $bedroomFilterMode = 'single';
    } else {
        $bedroomFilterMode = null;
    }

    if (isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range) {
        ++$filterGridCols;
        $bedroomFilterMode = 'multi';
    } else {
        $bedroomFilterMode = $bedroomFilterMode;
    }

    if (is_null($bedroomFilterMode)) {
        --$filterGridCols;
    }

    if(!isset($searchControls->bathrooms) || !$searchControls->bathrooms) {
        --$filterGridCols;
    }

    if(!isset($searchControls->status) || !$searchControls->status) {
        --$filterGridCols;
    }

    if(!isset($searchControls->name) || !$searchControls->name) {
        --$filterGridCols;
    }

    if(!isset($searchControls->reference) || !$searchControls->reference) {
        --$filterGridCols;
    }
@endphp

<section class="bg-gray-100 relative z-10">
    <!-- small and lower -->
    <div class="block lg:hidden w-full relative mt-6">
        @include(themeViewPath('frontend.components.search-filter-mobile'))
    </div>

    <div class="hidden lg:block py-6"> <!-- full search -->
        @include(themeViewPath('frontend.components.search-filter-desktop'))
    </div>
</section>
