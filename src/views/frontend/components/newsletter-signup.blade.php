<section class="relative z-10">
    <div class="container mx-auto">
    <form id="newsletter-signup" method="post" action="javascript:" enctype="application/x-www-form-urlencoded">
        <div class="container mx-auto px-4 my-2">
            <div class="grid grid-cols-1 xl:grid-cols-5 xl:gap-4">

                <div class="col-span-2 text-center pb-4 md:pb-0 pr-0 md:pr-4 text-center xl:text-right mb-2 md:mb-8 xl:mb-0">
                    <h3 class="header-text text-2xl md:text-3xl leading-tight mb-1 footer-text">{{ trans('mailing-list.signup_to_our_newsletter') }}</h3>
                    <span class="text-base leading-tight tracking-tight footer-text block">{!! trans('mailing-list.stay_upto_date_with_our_latest_news') !!}</span>
                </div>

                <div class="mb-4 xl:mb-0">
                    <input name="newsletter_name" type="text" class="text-sm md:text-base w-full rounded-full sm:rounded-r-none border-gray-50 border-2 px-4 sm:h-14 h-11 focus:border-0 focus:outline-none" placeholder="{{ trans('contact.full_name') }}" required>
                </div>

                <div class="col-span-2 relative">
                    <input name="newsletter_email" type="email" class="text-sm md:text-base w-full rounded-full sm:rounded-l-none border-gray-50 border-2 px-4 sm:h-14 h-11 focus:border-0 focus:outline-none" placeholder="Enter Email Address" required>
                    <button type="submit" class="text-sm md:text-base text-center tracking-wide font-bold text-white cta cta-text uppercase px-4 sm:px-9 rounded-full h-9 sm:h-12  right-1 top-1 transition-all absolute">{{ trans('mailing-list.signup') }}</button>
                </div>

            </div>
        </div>
        @csrf
    </form>
    </div>
</section>
