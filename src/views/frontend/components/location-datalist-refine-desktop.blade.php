@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $inputClass = 'w-full autocomplete-location lg:pl-2 border-l-0 pl-0 placeholder-black focus:outline-none text-base lg:text-sm xl:text-base ';
    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';
@endphp

<div class="w-full location-datalist-container">

    {{-- <input class="placeholder-black location-datalist-input {{ $inputClass }}"  autocomplete="off" role="combobox" list="" name="location" placeholder="{{ trans('placeholder.search_location') }}" value="{{ $locationValue }}"> --}}

    @include(themeViewPath('frontend.components.location-search-input'), ['datalist' => true, 'additionalClasses' => $inputClass, 'value' => $locationValue])

    <datalist id="browsers" role="listbox" class="absolute w-full text-xs top-14 left-0 " style="z-index: 999;">
        <div class="text-left text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-50 bg-white rounded-b-lg">
            @php $i = 1 @endphp
            @foreach($dataList as $text => $value)
                <option class="text-xs bg-white text-black cursor-pointer hover:bg-gray-100 py-2 px-4 @if($i === 1) pt-4 @endif" data-value="{{ $value }}">{{ $text }}</option>
                @php $i++ @endphp
            @endforeach
        </div>
    </datalist>

    <input type="hidden" name="location_url">
</div>
