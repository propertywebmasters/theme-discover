<section class="py-12">
    <div class="container mx-auto px-4">
        <div class="bg-white grid grid-cols-1 lg:grid-cols-2 gap-4 shadow-2xl rounded-xl">
            <div class="py-14 px-7 lg:px-14">
                <h3 class="text-xl lg:text-3xl leading-normal font-bold text-primary mb-9">{{ $branch->name }}</h3>
                <div>
                    <div class="flex items-center">
                        <img src="{{ themeImage('map-marker-alt.svg') }}" class="svg-inject primary-text fill-current" alt="marker" loading="lazy">
                        <address class="ml-2 text-base leading-tight tracking-tight inline-block not-italic text-left">{{ $branch->displayAddress(true) }}</address>
                    </div>
                    <div class="flex items-center py-4">
                        <img src="{{ themeImage('phone.svg') }}" class="svg-inject primary-text fill-current" alt="phone" loading="lazy">
                        <a href="tel:{{ $branch->tel }}" class="ml-2 text-base leading-tight tracking-tight inline-block">{{ $branch->tel }}</a>
                    </div>
                    <div class="flex items-center">
                        <img src="{{ themeImage('email2.svg') }}" class="svg-inject primary-text fill-current" alt="marker" loading="lazy">
                        <a href="mailto:{{ $branch->email }}" class="ml-2 text-base leading-tight tracking-tight inline-block">{{ $branch->email }}</a>
                    </div>
                    {{-- <a href="mailto:{{ $branch->email }}" class="text-base text-center tracking-wide font-bold text-white uppercase cta px-4 sm:px-9 rounded-full right-1 top-1 hover:bg-secondary transition-all inline-block py-3 mt-9"> --}}
                    <a href="mailto:{{ $branch->email }}" class="text-base text-center tracking-wide font-bold text-white uppercase cta px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 hover:bg-secondary transition-all inline-block mt-9 leading-9 sm:leading-12">
                        {{ trans('contact.contact_branch') }}
                    </a>
                </div>
            </div>
            <div class="mapouter relative text-right w-full h-80 lg:h-full">
                <iframe class="rounded-xl w-full h-full" id="gmap_canvas" src="https://maps.google.com/maps?q={{urlencode($branch->displayAddress(true))}}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
        </div>
    </div>
</section>
