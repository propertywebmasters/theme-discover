@php
$imageClasses = 'min-h-46 max-h-46 sm:min-h-46 sm:max-h-46 md:min-h-58 md:max-h-58 lg:min-h-68 lg:max-h-68 xl:min-h-68 xl:max-h-68'
@endphp
<div class="career-card rounded-lg @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div onclick="window.location='{{ localeUrl('/careers/'.$career->url_key) }}'" class="relative rounded-t-2xl overflow-hidden center-cover-bg bg-lazy-load cursor-pointer">
    </div>
    <div class="px-3 md:px-5 lg:px-7 py-7 shadow-md rounded-lg">
        <span class="text-base tracking-tight mb-4 inline-block">{{ $career->created_at->format('d/m/Y') }}</span>
        <div class="line-clamp-6">
            <a href="{{ localeUrl('/careers/'.$career->url_key) }}" class="text-base md:text-xl leading-tight font-bold block" title="{!! optional($data)->title !!}">{!! optional($data)->title !!}</a>
            <p class="text-sm md:text-base leading-tight text-justify">{!!  \Illuminate\Support\Str::limit(strip_tags(optional($data)->description), 360)  !!}</p>
        </div>
        <a href="{{ localeUrl('/careers/'.$career->url_key) }}" class="text-base tracking-wide font-bold primary-text uppercase block mt-7 transition-all inline-block">{{ trans('generic.read_more') }}</a>
        <span class="float-right font-bold inline-block mt-7">{{ $career->salary }}</span>
    </div>
</div>
