@php
$imageClasses = 'min-h-46 max-h-46 sm:min-h-46 sm:max-h-46 md:min-h-58 md:max-h-58 lg:min-h-56 lg:max-h-56'
@endphp
<div class="article-card rounded-lg @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div onclick="window.location='{{ localeUrl('/news/'.$article->url_key) }}'" class="relative rounded-t-2xl {{ $imageClasses }} overflow-hidden center-cover-bg bg-lazy-load cursor-pointer" data-style="background-image: url('{{ assetPath($article->image) }}'); background-position: center center;">
        <!-- -->
    </div>
    <div class="px-3 md:px-5 lg:px-7 py-7 shadow-md rounded-lg">
        <span class="text-base tracking-tight inline-block mb-2">{{ $article->created_at->format('d/m/Y') }}</span>
        <div class="line-clamp-6">
            <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="text-base md:text-xl leading-tight font-bold block mb-3" title="{{ $article->title }}">{{ $article->title }}</a>
            <p class="text-sm md:text-base leading-tight">{!!  \Illuminate\Support\Str::limit(strip_tags($article->content), 360)  !!}</p>
        </div>
        <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="text-base tracking-wide font-bold primary-text uppercase block mt-7 transition-all">{{ trans('generic.read_more') }}</a>
    </div>
</div>
