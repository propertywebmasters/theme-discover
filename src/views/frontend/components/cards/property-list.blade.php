@php
    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $infoWindowHtml = view('frontend.components.info-window', ['property' => $property])->render();
        $data =  'data-identifier="'.$property->uuid.'" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'" data-infowindow="'.str_replace("\n", "", htmlentities($infoWindowHtml)).'"';
    } else {
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }

    $descriptionSummary = null;
    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }

    if ($propertyDescription !== null) {
        $tmpDesc = strip_tags($propertyDescription->long);
        $descriptionSummary = \Illuminate\Support\Str::limit($tmpDesc, 300);
    }
@endphp

<div class="flex flex-wrap mb-11 bg-white border border-gray rounded-l-lg rounded-r-lg">

    <div class="w-full md:w-1/2 lg:w-5/12 xl:w-1/3 overflow-hidden @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY)) inline-listing-gallery @endif @if($splitMapMode) property-listing-map @endif {{ $class }} {{ $extraCss }}" {!! $data !!}>
        <div class="relative">

            @if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
                <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl">{{ $property->tender_status }}</div>
            @endif

            @php
                $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
                $bgImageClass = 'min-height: 300px; max-height: 300px; overflow-hidden background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl).'")';
            @endphp
            <div class="bg-lazy-load rounded-l-lg rounded-t-lg rounded-b-lg" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;</div>

            @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
                <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
                </div>
                <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
                </div>
            @endif
        </div>
    </div>

    <div class="w-full md:w-1/2 lg:w-7/12 xl:w-2/3 pl-4 lg:pl-10 py-6 pr-4 relative flex items-center">
        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                    $actionClass = 'modal-button';
                    $dataAttribs = 'data-target="preauth-modal"';
                    $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is not in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif

            <a href="javascript:" class="{{ $actionClass }}" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                <img src="{{ themeImage('heart.svg') }}" class="svg-inject {{ $imgClass }} absolute right-4 top-4 primary-text" alt="love" loading="lazy">
            </a>
        @endif

        <div class="py-4 md:py-0">
            <div>
                <a href="{{ localeUrl('/property/'.$property->url_key) }}" class="@if(!$splitMapMode) text-2xl block @else text-lg text-primary m-0 p-0 overflow-hidden block line-clamp-1 @endif">
                    {!! $property->displayName() !!}
                </a>

                <span class="text-sm tracking-tight text-gray-600 block mb-4 mt-2">{!! $property->location->displayAddress()  !!}</span>
                @php
                    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
                    if ($propertyDescription === null) {
                        $propertyDescription = $property->descriptions->first();
                    }
                @endphp

                @if ($descriptionSummary !== null && !$splitMapMode)
                    <p class="text-sm leading-normal tracking-tight font-light text-black line-clamp-3">
                        {!! $descriptionSummary !!}
                    </p>
                @endif

                <div class="mt-5 text-xl tracking-tight font-medium text-secondary-text uppercase">
                    {!! $property->displayPrice() !!}
                </div><!-- / -->
            </div>

            @include(themeViewPath('frontend.components.property.bed-bath-info'))
        </div>
    </div>
</div>
