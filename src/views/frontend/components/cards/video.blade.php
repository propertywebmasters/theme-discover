@php
    $embedUrl = $video->getEmbedUrl();
@endphp
<div class="rounded-b-2xl rounded-t-2xl w-full bg-white shadow-md @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div class="rounded-t-2xl min-h-80 sm:min-h-96 md:min-h-72 lg:min-h-80 xl:min-h-68 overflow-hidden">
        <iframe loading="lazy" class="w-full min-h-80 sm:min-h-96 md:min-h-72 lg:min-h-80 xl:min-h-68" style="width: 100%;" src="{{ $embedUrl }}" title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="p-8 text-center">
        <div class="py-0">
            <span class="block text-lg font-medium primary-text leading-5">{{ $video->title }}</span>
        </div>
    </div>
</div>
