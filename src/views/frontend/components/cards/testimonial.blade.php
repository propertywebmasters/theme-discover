<div class="swiper-slide">
    <div class="px-0 sm:px-4 p-6 text-center relative">
        <p class="text-lg leading-tight text-center tracking-tight"><span class="text-xl">&quot;</span>{!! $testimonial->content !!}<span class="text-xl">&quot;</span></p>
        <div class="w-full py-2">
            <span class="text-sm text-center tracking-tight primary-text inline-block block py-2">
                {{ $testimonial->company_name ? $testimonial->name . ', ' . $testimonial->company_name : $testimonial->name }}
            </span>
        </div>
    </div>
</div>

