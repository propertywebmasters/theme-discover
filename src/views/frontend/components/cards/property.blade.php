@php
    use App\Models\Property;use App\Models\TenantFeature;$splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $data =  'data-zoom="12" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'"';
    } else {
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }

    $cardClasses = 'min-h-68 h-68 max-h-68 overflow-hidden';

@endphp
<div class="relative rounded-lg property-listing inline-listing-gallery z-1 {{ $class }} {{ $extraCss }}" {{ $data }}>
    <div class="relative {{ $cardClasses }}">

        @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl">{{ $property->tender_status }}</div>
        @endif

        @php
            $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
            $bgImageClass = 'min-height: 300px; max-height: 300px; overflow: hidden; background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl).'")';
        @endphp

        <div class="bg-lazy-load rounded-l-lg rounded-t-lg rounded-b-lg" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}"
             onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;
        </div>

        @if(hasFeature(TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
            <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
            </div>
            <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
            </div>
            <a href="{{ localeUrl('/property/'.$property->url_key) }}" title="{{ $property->displayName() }}">
                <img id="property-{{ $property->uuid }}" class="property-card-image w-full h-full rounded-t-2xl object-cover"
                     src="{{ getPropertyImage($property->images->first()->filepath) }}"
                     alt="img" loading="lazy">
            </a>
        @endif

    </div>
    <div class="relative px-5 md:py-7 pt-4 pb-7 shadow-md rounded-lg" style="min-height: 16rem;">

        @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                    $actionClass = 'modal-button';
                    $dataAttribs = 'data-target="preauth-modal"';
                    $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif
            <div class="inline-block absolute bottom-10 right-5 mt-1">
                <a href="javascript:" class="{{ $actionClass }} h-8 w-8 primary-text" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                    <img src="{{ themeImage('heart.svg') }}" class="svg-inject {{ $imgClass }} inline-block" alt="love" loading="lazy">
                </a>
            </div>
        @endif

        <div style="min-height: 5rem;">
            <span class="text-xl leading-tight font-bold text-primary uppercase block line-clamp-1" style="min-height: 2rem;">
                {!! $property->displayPrice() !!}
            </span>

            <a class="text-lg text-primary m-0 p-0 overflow-hidden block line-clamp-1" href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->displayName() !!}</a>
            <span class="text-sm lg:text-base tracking-tight line-clamp-1">{!! $property->location->displayAddress()  !!} </span>
        </div>

        <div class="absolute left-5 bottom-10">
            @include(themeViewPath('frontend.components.property.bed-bath-info'), ['extraClasses' => 'mt-3'])

            <a href="{{ localeUrl('/property/'.$property->url_key) }}"
               class="text-sm md:text-base text-center tracking-wide font-bold uppercase text-white rounded-3xl shadow max-w-xs px-5 py-2 inline-block mt-6 transition-all cta cta-text">
                {{ trans('generic.view_details') }}
            </a>
        </div>
    </div>

</div>
