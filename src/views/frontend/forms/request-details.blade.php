<form action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required value="{{ optional(user())->name() }}">
    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required value="{{ optional(user())->email }}">
    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required value="{{ optional(user())->tel }}">
    <textarea name="comment" placeholder="{{ trans('placeholder.further_details') }}" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2"></textarea>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'block bg-gray-200 text-gray-900 block w-full my-2 p-2'])

    <p class="text-center text-sm pb-6 text-gray-500 modal-terms">{!! trans('terms.modal_terms') !!}</p>

    <div class="">
        <button type="submit" role="button"
                class="rounded-full w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">
            {{ trans('button.send_enquiry') }}
        </button>
    </div>
    @csrf
</form>
