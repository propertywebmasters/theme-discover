<form action="{{ localeUrl('/property/'.$property->url_key.'/viewing') }}"  method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">

    <div class="pb-6">
        <label for="viewing_date" class="block pb-2">{{ trans('label.choose_a_date') }} <span class="rqd-field">*</span></label>
        <input id="viewing_date" name="date" type="date" class="p-2 border border-gray-300 border-solid block w-full" required>
    </div>

    <div class="pb-6">
        <label for="viewing_slots" class="block pb-2">{{ trans('label.please_select_upto_3_slots') }} <span class="rqd-field">*</span></label>

        <div class="grid grid-cols-4 gap-2">
            {{-- @todo Replace this with a foreach dateInterval --}}
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">09:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">09:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">10:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">10:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">11:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">11:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">12:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">12:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">13:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">13:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">14:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">14:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">15:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">15:30</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">16:00</span></div>
            <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">16:30</span></div>
        </div>

        {{-- javascript builds up the value for this to be submitted --}}
        <input type="hidden" name="slots" value="">

    </div>

    <hr class="my-6">

    <div class="">
        <label for="viewing_slots" class="block pb-2">{{ trans('label.please_fill_in_your_details_below') }}</label>
        <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required>
        <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required>
        <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }} *" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2" required>
        <textarea name="comment" placeholder="{{ trans('placeholder.further_details') }}" class="block bg-gray-200 text-gray-900 block w-full my-2 p-2"></textarea>

        @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'block bg-gray-200 text-gray-900 block w-full my-2 p-2'])

        <p class="text-center text-sm pb-6 text-gray-500 modal-terms">{!! trans('terms.modal_terms')  !!}</p>
    </div>

    <div class="">
        <button type="submit" role="button"
                class="w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer rounded-full">
            {{ trans('button.request_viewing') }}
        </button>
    </div>
    @csrf
</form>
