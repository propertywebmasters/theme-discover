@php
    if ($searchRequest->get('tender_type') === null) {
        $isSale = stripos($_SERVER['REQUEST_URI'], 'properties-for-sale') !== false;
        $isRental = !$isSale;
    } else {
        $isRental = in_array($searchRequest->get('tender_type'), [
            'rental',
            'short_term_rental',
            'long_term_rental'
        ]);
        $isSale = $searchRequest->get('tender_type') === 'sale' || !$isRental;
    }
@endphp
<form action="{{ localeUrl('/account/alerts') }}" method="post" enctype="application/x-www-form-urlencoded">
    <div class="grid grid-cols-2 gap-2">
        <div class="col-span-2 relative">
            <input id="search_location" type="text" name="location" placeholder="{{ trans('placeholder.location') }}"
                   class="autocomplete-location bg-gray-200 p-2 w-full"
                   autocomplete="off"
                   value="{{ $searchRequest->get('location') }}"
                   data-search-results-container="desktop-search-results-container"
                   data-target-div="div#autocomplete-results"
                   readonly="readonly"
                   required/>

            <input type="hidden" name="location_url" value="{{ $searchRequest->get('location_url') }}">
        </div>
        <div>
            <select id="property_type" name="property_type" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Property Type</option>
                @foreach($propertyTypes as $propertyType)
                    @php
                        $selected = old('property_type', $searchRequest->get('property_type')) == strtolower($propertyType->name) ? ' selected' : ''
                    @endphp
                    <option value="{{ $propertyType->name }}"{{ $selected }}>{{ $propertyType->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <select id="property-alert-tender-type" name="tender_type" class="form-control bg-gray-200 p-2 w-full">
                <option value="sale" @if (old('tender_type', $searchRequest->get('type')) === 'sale') selected @endif>Sale</option>
                <option value="rental" @if (old('tender_type', $searchRequest->get('type')) === 'rental') selected @endif>Rental</option>
            </select>
        </div>
        <div>
            <select id="bedrooms" name="bedrooms" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Num Bedrooms</option>
                @foreach ($bedroomsRange as $bedroomCount)
                    @php
                        $selected = old('bedrooms', $searchRequest->get('bedrooms')) != 0 && old('bedrooms', $searchRequest->get('bedrooms')) == $bedroomCount ? ' selected' : ''
                    @endphp
                    <option value="{{ $bedroomCount }}"{{ $selected }}>Minimum {{ $bedroomCount }} Bedroom(s)</option>
                @endforeach
            </select>
        </div>
        <div>
            <select id="bathrooms" name="bathrooms" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Num Bathrooms</option>
                @foreach ($bathroomsRange as $bathroomCount)
                    @php
                        $selected = old('bathrooms', $searchRequest->get('bathrooms')) != 0 && old('bathrooms', $searchRequest->get('bathrooms')) == $bathroomCount ? ' selected' : ''
                    @endphp
                    <option value="{{ $bathroomCount }}"{{ $selected }}>Minimum {{ $bathroomCount }} Bathroom(s)</option>
                @endforeach
            </select>
        </div>


        <div class="sale_price_filter @if($isRental) hidden @endif">
            <select id="sale_min_price" name="sale_min_price" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Min Price</option>
                @foreach($searchPriceRanges['sale'] as $price)
                    <option value="{{ $price }}" @if($searchRequest->get('min_price') !== null && $searchRequest->get('min_price') == $price) selected @endif>{{ number_format($price) }} {{ $defaultCurrency->iso }}</option>
                @endforeach
            </select>
        </div>
        <div class="sale_price_filter @if($isRental) hidden @endif">
            <select id="sale_max_price" name="sale_max_price" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Max Price</option>
                @foreach($searchPriceRanges['sale'] as $price)
                    <option value="{{ $price }}" @if($searchRequest->get('min_price') !== null && $searchRequest->get('max_price') == $price) selected @endif>{{ number_format($price) }} {{ $defaultCurrency->iso }}</option>
                @endforeach
            </select>
        </div>
        <div class="rental_price_filter @if($isSale) hidden @endif">
            <select id="rental_min_price" name="rental_min_price" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Min Price2</option>
                @foreach($searchPriceRanges['rental'] as $price)
                    <option value="{{ $price }}"
                            @if($searchRequest->get('min_price') !== null && $searchRequest->get('min_price') == $price) selected @endif>{{ number_format($price) }} {{ $defaultCurrency->iso }} {{ $defaultRentalFrequency }}</option>
                @endforeach
            </select>
        </div>
        <div class="rental_price_filter @if($isSale) hidden @endif">
            <select id="rental_max_price" name="rental_max_price" class="form-control bg-gray-200 p-2 w-full">
                <option value="">Max Price</option>
                @foreach($searchPriceRanges['rental'] as $price)
                    <option value="{{ $price }}"
                            @if($searchRequest->get('min_price') !== null && $searchRequest->get('max_price') == $price) selected @endif>{{ number_format($price) }} {{ $defaultCurrency->iso }} {{ $defaultRentalFrequency }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <p class="text-center text-sm py-4 text-gray-500 modal-terms">{!! trans('terms.modal_terms') !!}</p>

    <div class="text-right">
        <button type="submit" role="button"
                class="primary-bg block w-full text-center py-4 px-10 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer rounded-full">
            Create Alert
        </button>
    </div>
    @csrf
</form>
