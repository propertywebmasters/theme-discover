@php
    $class = 'bg-transparent text-black placeholder-gray-800 border-b border-gray-400 block w-full py-2 text-sm mb-8 focus:outline-none';
    $fallbackQuestions = getRecaptchaFallbackQuestionAnswerSet();
    $randomQuestion = array_rand($fallbackQuestions, 1)
@endphp
<form class="recaptcha" action="{{ localeUrl('/property/'.$property->url_key.'/brochure-request') }}" method="post" enctype="application/x-www-form-urlencoded">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="{{ $class }}" value="{{ optional(user())->name() }}" required>
    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="{{ $class }}" value="{{ optional(user())->email }}" required>
    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}" class="{{ $class }}">
    <textarea name="comment" placeholder="{{ trans('contact.your_message') }}" class="{{ $class }}" rows="6" required></textarea>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => $class])

    <p class="text-xs text-center modal-terms">{!! trans('terms.modal_terms')  !!}</p>

    <div class="text-center mx-auto mt-4">
        <input type="submit" class="rounded-full relative cta font-bold py-4 px-12 text-sm uppercase cursor-pointer inline-block " value="{{ trans('generic.download_pdf') }}">
    </div>
    @csrf
</form>

