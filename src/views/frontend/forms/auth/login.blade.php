<div class="relative w-auto mt-6 mx-auto max-w-xl">
    <!--content-->
    <div class="relative border-0 shadow-lg relative flex flex-col w-full outline-none focus:outline-none">

        <button data-target="preauth-modal" class="modal-close focus:outline-none absolute top-4 right-4 z-10">
            <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
        </button>

        <div class="preauth-modal-wrapper">
            <div class="modal-box-body bg-white text-center py-8 xl:px-16">
                <h3 class="text-xl lg:text-3xl leading-relaxed text-center header-text">{!! translatableContent('login-modal', 'modal-title') !!}</h3>
                <span class="text-gray-700">{!! translatableContent('login-modal', 'modal-subtitle') !!}</span>

                <span id="login-error" class="block text-red-500 mt-6 hidden"></span>

                <div class="preauth-replaceable">
                    <form id="preauth-login-form" class="mt-6 py-2 px-4" action="javascript:" method="post" enctype="application/x-www-form-urlencoded" data-locale="{{ app()->getLocale() }}">
                        <input id="preauth-password" type="password" name="password" placeholder="Password" class="block bg-gray-200 text-gray-900 block w-full my-3 p-2" autocomplete="off" required>
                        <input type="hidden" name="email" value="{{ $email }}">
                        <button type="submit" role="button" class="rounded-full w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">Submit &rarr;</button>
                        @csrf
                    </form>
                </div>

                <div class="preauth-loader hidden">
                    <div class=" pt-8 w-32 mx-auto text-center primary-text">
                        @include('frontend.components.loader')
                    </div>
                </div>

                {{-- <hr class="my-6">--}}
                {{-- <a id="forgotten-password" class="block mt-4 cursor-pointer" href="javascript:;">{!! translatableContent('login-modal', 'modal-forgotten-password-text') !!}</a>--}}

            </div>

        </div>

    </div>
</div>
