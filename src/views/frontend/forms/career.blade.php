@php
    $class = 'rounded-full bg-white h-14 w-full px-4 mb-3 border';
    $fallbackQuestions = getRecaptchaFallbackQuestionAnswerSet();
    $randomQuestion = array_rand($fallbackQuestions, 1)
@endphp
<form class="recaptcha" action="{{ localeUrl('/careers/'.$career->url_key) }}" method="post" enctype="multipart/form-data">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="{{ $class }}" value="{{ optional(user())->name() }}" required>
    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="{{ $class }}" value="{{ optional(user())->email }}" required>
    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}" class="{{ $class }}" required>

    <div class="h-14 w-full mb-8">
        <label class="block mb-2 font-medium text-gray-900 dark:text-gray-300" for="file_input">{{ trans('career.upload_cv') }}</label>
        <input class="bg-transparent text-black block w-full focus:outline-none" type="file" name="cv">
    </div>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => $class])

    <p class="text-xs text-center modal-terms">{!! trans('terms.modal_terms')  !!}</p>

    <div class="text-center mx-auto mt-4">
        <input type="submit" class="text-base text-center tracking-wide font-bold text-white uppercase cta px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 hover:bg-secondary transition-all uppercase" value="{{ trans('career.send_details') }}">
    </div>
    @csrf
</form>

