<form action="javascript:" class="">
    <div class="-mx-3 md:flex mb-6">
        <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label for="name" class="block mb-3 text-md font-medium text-black-600">{{ trans('calculator.property_price') }}</label>
            <input type="number" name="price" placeholder="500000" step="1000" required class="w-full px-3 py-2 border h-14">
        </div>
        <div class="md:w-1/2 px-3">
            <label for="name" class="block mb-3 text-md font-medium text-black-600">{{ trans('calculator.deposit_payment') }}</label>
            <input type="number" name="deposit" placeholder="20000" step="1000" required class="w-full px-3 py-2 border h-14">
        </div>
    </div>

    <div class="-mx-3 md:flex mb-6 mt-10">
        <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label for="name" class="block mb-3 text-md font-medium text-black-600">{{ trans('calculator.repayment_period_years') }}</label>
            <input type="number" name="period" value="25" step="1" required class="w-full px-3 py-2 border h-14">
        </div>
        <div class="md:w-1/2 px-3">
            <label for="name" class="block mb-3 text-md font-medium text-black-600">{{ trans('calculator.annual_interest') }} (%)</label>
            <input type="number" name="interest" value="2.75" step="0.001" required class="w-full px-3 py-2 border h-14">
        </div>
    </div>

    <div class="w-full">
        <button id="calculate-mortgage"
                class="rounded-full w-full primary-bg block text-center py-4 text-xs sm:text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer hover-lighten">
            {{ trans('calculator.calculate') }}
        </button>
    </div>

</form>
<div id="mortgage-calculator-results" class="hidden bg-gray-200 pb-12 px-5 text-center relative mt-4" data-currency-symbol="{{ currentCurrency()->symbol }}">
    <img src="{{ themeImage('icons/triangle.png') }}" alt="triangle" class="absolute t-0 left-1/2 transform -translate-x-1/2">
    <div class="pt-8">
        <span class="repayment-amount pt-12 text-4xl font-semibold uppercase primary-text">£0</span>
        <span class="text-xl leading-tight text-center font-semibold lowercase primary-text">{{ trans('calculator.per_month') }}</span>
    </div>
</div>
