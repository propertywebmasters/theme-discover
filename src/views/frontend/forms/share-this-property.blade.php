<form action="{{ localeUrl('/property/'.$property->url_key.'/share') }}" method="post" class="recaptcha">
    @csrf

    <div class="mb-6">
        <label for="name" class="block mb-3 text-md font-medium text-black-600">Your details</label>

        <div>
            <div class="mb-4">
                <input id="sender-name" type="text" name="sender_name" placeholder="Full name" required class="w-full px-3 py-2 border h-14">
            </div>

            <div>
                <input id="sender-email" type="email" name="sender_email" placeholder="Email address" required class="w-full px-3 py-2 border h-14">
            </div>
        </div>
    </div>

    <div class="mb-6">
        <label for="recipient-name" class="block mb-3 text-md font-medium text-black-600">Your friend's details</label>

        <div>
            <div class="mb-4">
                <input id="recipient-name" type="text" name="recipient_name" placeholder="Full name" required class="w-full px-3 py-2 border h-14">
            </div>

            <div>
                <input id="recipient-email" type="email" name="recipient_email" placeholder="Email address" required class="w-full px-3 py-2 border h-14">
            </div>
        </div>
    </div>

    <div class="mb-6">
        @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'w-full px-3 py-2 border h-14'])
    </div>

    <input type="hidden" name="url" value="{{ url()->full() }}">

    <button type="submit" role="button" class="rounded-full w-full primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer">Share</button>
</form>
