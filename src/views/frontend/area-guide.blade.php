@extends('layouts.app')

@section('content')

    @php
        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="bg-whiter pb-24">
        <div class="container mx-auto px-4 pt-16">
            <h2 class="header-text py-9 pb-20 text-2xl lg:text-3xl xl:text-5xl text-center tracking-tight text-primary max-w-xl mx-auto">{{ $areaGuide->main_title }}</h2>
            <div class=" relative flex items-center justify-center">
                <div class="text-center">
                    <img class="rounded-2xl object-cover" src="{{ themeImage('area/1.jpg') }}" alt="1">
                </div>

                <form id="home-search" action="{{localeUrl('/search')}}" method="post" enctype="application/x-www-form-urlencoded" class="w-full absolute max-w-xl">
                    <div class="flex w-full flex-col sm:flex-row">

                        @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                            <div class="border-b md:border-none w-auto md:min-w-12 lg:min-w-24">
                                <select name="type" id="search_type" class="sm:rounded-r-none rounded-full h-14 px-10 text-browngrey appearance-none select-carat hidden sm:block" style="background-image: url('{{ themeImage('arrow-back.svg') }}'); background-repeat: no-repeat; background-position: 97% 25px;">
                                    <option value="sale">{{ trans('label.sale') }}</option>
                                    <option value="rental">{{ trans('label.rental') }}</option>
                                </select>
                            </div>
                        @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                            @if($searchControls->tender_type === 'sale_only')
                                <input id="search_type" name="type" type="hidden" value="sale">
                            @elseif($searchControls->tender_type === 'rental_only')
                                <input id="search_type" name="type" type="hidden" value="rental">
                            @endif
                        @endif

                        <div class="relative w-full mt-2 sm:mt-0">
                            <div class="absolute top-2 sm:h-10 h-7 w-0.5 bg-gray-300 left-2 opacity-0 sm:opacity-100 z-10">
                            </div>
                            <div class="bg-white w-full rounded-full h-11 sm:h-14">
                                <div class="flex items-center h-full">
                                    <div style="flex: 1;">
                                        @include(themeViewPath('frontend.components.location-search'), [
                                            'additionalClasses' => 'sm:rounded-l-none rounded-full h-11 sm:h-14 sm:px-10 focus:border-0 md:text-base text-sm px-3',
                                            'value' => $areaGuide->location,
                                            'placeholder' => '',
                                            'areaGuide' => true
                                        ])
                                    </div>
    
                                    <button type="submit"
                                            class="text-base text-center tracking-wide font-bold text-white uppercase cta px-4 sm:px-9 rounded-full h-9 sm:h-12 mr-1 hover-primary-bg transition-all">
                                        {{ trans('label.search') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </section>

    <section class="bg-white pt-16">
        <div class="container mx-auto px-4 pb-16">
            <div class="max-w-5xl mx-auto">
                <h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ $areaGuide->main_title }}</h3>
                <p class="text-base leading-normal tracking-tight">{!! $areaGuide->main_content !!}</p>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 pt-16 pb-8">
                <div>
                    <img class="w-full object-cover" src="{{ themeImage('area/2.jpg') }}" alt="img">
                </div>
                <div>
                    <img class="w-full object-cover" src="{{ themeImage('area/3.jpg') }}" alt="img">
                </div>
            </div>

        </div>
    </section>

{{--    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.similar_properties'), 'properties' => $relatedProperties])--}}

    @if(!$areaGuide->amenities->isEmpty())
        <div class="bg-gray-100">
            <div class="container mx-auto py-4 px-4 lg:px-8 lg:pt-16">
                <div class="grid grid-cols-1 lg:grid-cols-2 gap-2 xl:gap-16">
                    <div>
                        <div id="map" class="w-full md:h-96 h-full" data-zoom="12" data-latitude="{{ $areaGuide->latitude }}" data-longitude="{{ $areaGuide->longitude }}"></div>
                    </div>
                    <div>
                        <h3 class="text-2xl font-medium pb-8 text-center lg:text-left">{{ trans('area-guide.amenities_in_this_area') }}</h3>
                        <div class="grid grid-cols-1 lg:grid-cols-2 gap-8 lg:gap-12">
                            @foreach($areaGuide->amenities as $amenity)
                                <div>
                                    <span class="block font-medium text-xl text-center lg:text-left">{{ $amenity->title }}</span>
                                    <span class="text-center lg:text-left">{{ $amenity->content }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @include(themeViewPath('frontend.components.latest-news'), ['customHeader' => $newsIsRelated ? trans('header.latest_news_in').' '.$areaGuide->location : trans('header.latest_news')])

    @if ($areaGuide->footer_title !== null && $areaGuide->footer_content !== null)
        <div class="bg-gray-100">
            <div class="container mx-auto py-8 lg:py-16 px-4 lg:px-8">

                @if($areaGuide->youtube_id)
                    <div class="grid grid-cols-1 lg:grid-cols-3 gap-2 xl:gap-16">
                        <div class="text-center md:text-left">
                            <h3 class="text-3xl font-medium pb-8 primary-text">{{ $areaGuide->footer_title }}</h3>
                            <p class="pb-8 text-center md:text-left">{!! $areaGuide->footer_content !!}</p>
                            <a href="{{ $areaGuide->properties_in_location_link }}" target="_blank"
                               class="inline primary-bg text-center py-4 px-6 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer w-auto hover-lighten">
                                {{ trans('area-guide.view_properties_in_location') }} &rarr;
                            </a>
                        </div>
                        <div class="col-span-2 mt-8 lg:mt-0 mb-2 relative" style="padding-top: 56.25%">
                            <iframe class="w-full h-full absolute inset-0" src="https://www.youtube.com/embed/{{ $areaGuide->youtube_id }}" title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                @else
                    <div class="grid grid-cols-1">
                        <div>
                            <h3 class="text-3xl font-medium pb-8">{{ $areaGuide->footer_title }}</h3>
                            <p class="pb-8">{!! $areaGuide->footer_content !!}</p>
                            <a href="{{ $areaGuide->properties_in_location_link }}" target="_blank"
                               class="inline primary-bg text-center py-4 px-6 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer w-auto hover-lighten">
                                {{ trans('area-guide.view_properties_in_location') }} &rarr;
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endif

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
