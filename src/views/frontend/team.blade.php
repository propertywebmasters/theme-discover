@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="latest-news">
        <div class="container py-8 pb-12 px-8 xl:px-4 mx-auto">

            <div>
                <h2 class="text-2xl md:text-3xl pb-2 font-medium py-6 header-text">{{ trans('header.meet_the_team') }}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                <a href="/" class="primary-text">{{ trans('header.home') }}</a> &gt; {{ trans('header.meet_the_team') }}
            </div>

            @foreach ($departments as $department)
                @if ($department->members_count > 0)
                    <div class="mb-6">
                        <h2 class="text-xl md:text-2xl font-medium mb-3">{{ $department->name }}</h2>
                        <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8">
                            @foreach($department->members as $teamMember)
                                @include(themeViewPath('frontend.components.cards.team-member'), ['teamMember' => $teamMember])
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
