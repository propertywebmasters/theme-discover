@extends('layouts.app')

@section('content')

    @push('open-graph-tags')
        @include(themeViewPath('frontend.components.home-open-graph'))
    @endpush

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    {{-- Primary hero element --}}
    @include(themeViewPath('frontend.components.hero.hero-media'))

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.about'))

    {{-- Featured properties band --}}
    <div class="bg-white">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.featured_properties'), 'properties' => $featuredProperties])
    </div>

    {{-- Popular searches band --}}
    @include(themeViewPath('frontend.components.popular-searches'))

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.features'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.latest-videos'))

    {{-- Featured developments band --}}
{{--    @include(themeViewPath('frontend.components.featured-developments'))--}}

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.latest-news'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
