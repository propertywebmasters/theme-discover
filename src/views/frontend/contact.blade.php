@extends('layouts.app')

@section('content')

    @php
    use App\Models\SiteSetting;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;$siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key'])
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="site-contact" class="py-16 mt-16 center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('contact.hero') }}">
        <div class="container mx-auto px-4 center-cover-bg bg-lazy-load">

            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            <div class="grid grid-cols-1 md:grid-cols-2 gap-8">

                <div>
                    <div class="mx-auto md:mt-48">
                        <div class="">
                            <div class="navigation-bg rounded-t-lg mx-auto p-12">
                                <h2 class="text-2xl lg:text-3xl xl:text-5xl header-text text-left tracking-tight text-white mx-auto max-w-xl">{!! dynamicContent($pageContents, 'contact-title') !!}</h2>
                            </div>
                            <div class="primary-bg rounded-b-lg py-8">
                                <div class="text-xl tracking-tight justify-center contact-quick-info grid grid-cols-1 xl:grid-cols-2">
                                    <a href="mailto:{{ $siteEmail }}" class="flex text-white mb-2 ml-12">
                                        <img src="{{ themeImage('email2.svg') }}" class="svg-inject text-white fill-current mr-2 mt-2" alt="marker" loading="lazy"> {{ $siteEmail }}
                                    </a>
                                    <a href="tel:{{ $siteTel }}" class="flex lg:ml-6 text-white ml-12">
                                        <img src="{{ themeImage('phone.svg') }}" alt="img" class="svg-inject text-white fill-current mr-2 mt-2" loading="lazy"> {{ $siteTel }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="mx-auto py-9 px-4 lg:px-11 bg-white rounded-2xl mt-0 md:mt-16">
                        @include(themeViewPath('frontend.components.forms.contact-form'))
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.contact-branches'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
