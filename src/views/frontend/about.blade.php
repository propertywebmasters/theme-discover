@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="bg-whiter pb-24">
        <div class="container mx-auto px-4 pt-32 lg:pt-48">
            <h2 class="header-text text-4xl xl:text-5xl tracking-tight text-primary mb-4 lg:mb-9 mx-auto text-center">{!! translatableContent('about', 'about-title') !!}</h2>
            <p class="pb-12 xl:pb-20 text-lg xl:text-xl text-center tracking-tight">{!! translatableContent('about', 'about-subtitle') !!}</p>
            <div class="text-center">
                <img class="rounded-2xl w-full min-h-80 md:min-h-114 xl:min-h-154 object-cover" src="{{ assetPath(translatableContent('about', 'about-header-image')) }}" alt="1">
            </div>

        </div>
    </section>

    <section class="bg-white pt-16">
        <div class="container mx-auto px-4 border-b-2 pb-16">
            <div class="max-w-5xl mx-auto">
                <h3 class="header-text text-2xl leading-loose tracking-tight font-bold text-primary mb-6">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                <p class="text-base leading-normal tracking-tight">{!! translatableContent('about', 'about-section-1-text') !!}</p>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 py-16">
                <div>
                    <img class="w-full object-cover" src="{{ assetPath(translatableContent('about', 'about-additional-image-1')) }}" alt="img">
                </div>
                <div>
                    <img class="w-full object-cover" src="{{ assetPath(translatableContent('about', 'about-additional-image-2')) }}" alt="img">
                </div>
            </div>
            <div class="max-w-5xl mx-auto">
                <h3 class="header-text text-2xl leading-loose tracking-tight font-bold text-primary mb-6">{!! translatableContent('about', 'about-section-2-title') !!}</h3>
                <p class="text-base leading-normal tracking-tight">{!! translatableContent('about', 'about-section-2-text') !!}</p>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.latest-news'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
