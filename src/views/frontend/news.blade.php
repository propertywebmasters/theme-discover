@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="news-archive" class="pt-7">
        <div class="container px-8 xl:px-4 mx-auto">

            @include('frontend.components.system-notifications', ['customClass' => 'mb-6'])

            <div>
                <h2 class="text-2xl md:text-4xl pb-2 font-medium py-6 header-text">{{ $header }}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                <a href="{{ localeUrl('/') }}" class="primary-text">{{ trans('header.home') }}</a> &gt; {{ trans('header.latest_news') }} @if(request()->has('tag')) &gt; {{ $header }} @endif
            </div>

            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8">
                @foreach($articles as $article)
                    @include(themeViewPath('frontend.components.cards.article'), ['article' => $article])
                @endforeach
            </div>

        </div>

    </section>

    @if($articles->links())
        @include(themeViewPath('frontend.components.listings.listings-pagination'), ['data' => $articles, 'extraClass' => 'mt-12'])
    @endif

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
