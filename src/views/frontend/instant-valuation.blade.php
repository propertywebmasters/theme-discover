@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="instant-valuation" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('instant-valuation.hero') }}">
        <div class="pt-8 pb-40">
            <div class="container mx-auto transparent pt-12 px-8 lg:px-32 xl:px-64 pb-24 text-center">
                <h1 class="text-white text-5xl mx-auto py-6 header-text relative top-12 md:top-0">{!! dynamicContent($pageContents, 'instant-valuation-intro-title') !!}</h1>
            </div>
        </div>
    </section>
    <div class="container mx-auto px-8 lg:px-32 xl:px-64 pb-16 -mt-44">
        <div class="">
            @include(themeViewPath('frontend.forms.instant-valuation'), [
                'inputClasses' => 'rounded-lg px-4',
                'submitClasses' => 'rounded-lg',
                'boxClasses' => 'rounded-lg',
            ])
        </div>
    </div>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
