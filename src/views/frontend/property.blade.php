@php use App\Models\Property; @endphp
@php use Illuminate\Support\Str; @endphp
@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.property.open-graph'))
@endpush

@section('content')
    @php
        $imageString = $captionString = '';
        foreach ($property->images as $image) {
            $imageString .= getPropertyImage($image->filepath, 'xl').'|';
            $captionString .= $image->caption.'|';
        }

        $imageString = trim($imageString, '|');
        $captionString = trim($captionString, '|');
        $imageCount = $property->images->count()
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="property-detail" class="bg-whiter">
        <div class="bg-white py-2">
            <div class="container mx-auto flex justify-between items-center px-4 md:px-0 xl:px-4">
                @if(stripos(url()->previous(), tenantManager()->getTenant()->domain) !== false || stripos(url()->previous(), tenantManager()->getTenant()->dev_domain) !== false)
                    <div class="flex">
                        <a href="{{ url()->previous() }}" class="leading-normal block">
                            <img src="{{ themeImage('icons/angle-right-black.png') }}" alt="" class="inline-block transform rotate-180" loading="lazy">
                            <span class="inline-block text-sm  leading-normal font-medium pl-1">{{ trans('search.back_to_search') }}</span>
                        </a>
                    </div>
                @endif

                <span class="tracking-tight text-sm">{{ trans('label.reference') }}: {{ $property->displayReference() }}</span>
            </div>
        </div>

        <div class="container px-4 md:px-0 xl:px-4 mx-auto">

            <div class="flex flex-wrap pt-4 xl:pt-8">

                <div class="w-full xl:w-2/6 pb-3 xl:pb-10 py-0 pr-4">
                    <div class="xl:mt-20">

                        <div class="pr-1">

                            @include(themeViewPath('frontend.components.system-notifications'))

                            <div>
                                <h1 class="header-text mt-3 block text-2xl xl:text-3xl leading-tight tracking-tight text-primary">{{ $property->displayName() }}</h1>
                                <p class="text-base xl:text-lg tracking-tight mt-1 xl:mt-3">{{ $property->location->displayAddress() }}</p>

                                @include(themeViewPath('frontend.components.property.bed-bath-info'), ['extraClasses' => 'bed-bath-info', 'showSizing' => true])
                            </div>

                            <h3 class="text-2xl leading-loose font-bold primary-text uppercase ">{!! $property->displayPrice() !!}</h3>

                            <div class="flex flex-wrap items-center justify-start">
                                @if(hasFeature(TenantFeature::FEATURE_ARRANGE_VIEWING_SYSTEM))
                                    <div class="text-center md:text-left mr-2 mb-2">
                                        <a href="javascript:" data-target="book-viewing-modal"
                                           class="leading-tight modal-button inline-block text-xs uppercase border rounded-3xl py-2 px-4 primary-text primary-border">{{ trans('header.book_viewing')}}</a>
                                    </div>
                                @endif
                                @if(hasFeature(TenantFeature::FEATURE_MORTGAGE_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                                    <div class="text-center md:text-left mr-2 mb-2">
                                        <a href="javascript:"
                                           class="leading-tight modal-button inline-block text-xs uppercase border rounded-3xl py-2 px-4 primary-text primary-border"
                                           data-target="mortgage-calculator-modal">{{ trans('header.mortgage_calculator') }}</a>
                                    </div>
                                @endif
                                @if(hasFeature(TenantFeature::FEATURE_STAMP_DUTY_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                                    <div class="text-center md:text-left mb-2">
                                        <a href="javascript:"
                                           class="leading-tight modal-button inline-block text-xs uppercase border rounded-3xl py-2 px-4 primary-text primary-border"
                                           data-target="stamp-duty-calculator-modal">{{ trans('header.stamp_duty') }}</a>
                                    </div>
                                @endif
                            </div>

                            @php $buttonClasses = 'block align-middle w-full text-sm xl:text-base text-center tracking-wide font-bold uppercase rounded-full transition-all py-4 primary-bg text-white' @endphp

                        </div>

                        <div class="lg:mt-12 hidden xl:block">
                            <div class="grid grid-cols-2 gap-4 mt-4 lg:mt-6">
                                <div class="table-cell align-middle">
                                    <a href="javascript:" data-target="property-enquiry-form"
                                       class="{{ $buttonClasses }} cta cta-text scroll-to hidden lg:block">{{ trans('button.enquire_now') }}</a>
                                    <a href="javascript:" data-target="request-details-modal"
                                       class="modal-button {{ $buttonClasses }} cta cta-text lg:hidden">{{ trans('button.enquire_now') }}</a>
                                </div>

                                <div class="table-cell align-middle">
                                    @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                                        @if(user())
                                            @php
                                                $shortlistText = in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())
                                                    ? trans('shortlist.saved')
                                                    : trans('shortlist.save');

                                                $noFill = !in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray());
                                            @endphp
                                            <a href="{{ localeUrl('/property/'.$property->url_key.'/toggle-shortlist') }}" class="{{ $buttonClasses }}" style="max-width: 200px;">
                                                <span class="inline-block mr-2">
                                                    <img class="svg-inject fill-current stroke-current h-5" src="{{ themeImage('heart.svg') }}" alt=""
                                                         @if ($noFill) style="fill: none;" @endif>
                                                </span>
                                                <span class="inline-block">{{ $shortlistText }}</span>
                                            </a>
                                        @else
                                            <a href="javascript:" data-target="preauth-modal"
                                               class="modal-button {{ $buttonClasses }} flex items-center justify-center" style="max-width: 200px;">
                                                <span class="inline-block mr-2">
                                                    <img class="svg-inject fill-current stroke-current h-5" src="{{ themeImage('heart.svg') }}" alt="" style="fill: none;">
                                                </span>
                                                <span class="inline-block">
                                                    {{ Str::limit(trans('shortlist.save'), 14) }}
                                                </span>
                                            </a>
                                        @endif
                                    @endif
                                </div>

                                @if(user())
                                    <div class="table-cell align-middle">
                                        <a href="javascript:"
                                           data-target="property-note-modal"
                                           class="modal-button {{ $buttonClasses }} cta cta-text scroll-to">
                                            {{ trans('button.add_property_note') }}
                                        </a>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full xl:w-4/6 xl:pl-12 pb-8 relative">
                    <div class="relative">
                        <div class="swiper custom-swiper-gallery-main overflow-hidden md:p-0 z-1 list-none relative">

                            @php
                                $mainClasses = 'h-60 sm:h-80 md:h-104 lg:h-114 xl:h-140';
                                $thumbClasses = 'h-16 mx-auto overflow-hidden object-cover w-full'
                            @endphp

                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content slider {{ $mainClasses }}" data-images="{{ $imageString }}"
                                 data-captions="{{ $captionString }}">

                                @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
                                    <div
                                        class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl z-10">{{ $property->tender_status }}</div>
                                @endif

                                @if ($imageCount)
                                    @foreach($property->images as $image)
                                        <div class="swiper-slide flex-shrink-0 cursor-pointer start-gallery" data-img-src="{{ getPropertyImage($image->filepath, 'xl') }}">
                                            <img src="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img" loading="lazy" class="w-full object-cover {{ $mainClasses }}">
                                        </div>
                                    @endforeach
                                @else
                                    <div class="swiper-slide">
                                        <img id="gallery-image-focus" src="/img/image-coming-soon.jpg" alt="img" loading="lazy" class="w-full cursor-pointer">
                                    </div>
                                @endif
                            </div>

                            <div class="sl-btns absolute bottom-2 left-2 md:bottom-7 md:left-4 xl:bottom-4 xl:left-4 flex z-30">
                                @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-location-modal">{{ trans('header.location') }}</a>
                                @endif
                                @if (!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
                                    @if ($property->documents->count() > 0)
                                        <a href="javascript:"
                                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                           data-target="property-documents-modal">{{ trans('header.documents') }}</a>
                                    @endif
                                @else
                                    @include(themeViewPath('frontend.components.property.document-by-type-buttons'))
                                @endif
                                @if ($property->video_url)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-video-modal">{{ trans('header.video') }}</a>
                                @endif
                                @if ($property->virtual_tour_url)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-virtual-tour-modal">{{ trans('header.virtual_tour') }}</a>
                                @endif
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div>

                        <div class="absolute inset-y-1/2 w-full -translate-y-1/2 flex justify-between lg:block z-40">
                            <div class="swiper-button-prevBtn relative md:absolute md:left-4 top-0 cursor-pointer inline-block pr-2 md:pr-0">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy"
                                     style="transform: rotate(180deg);">
                            </div>
                            <div class="swiper-button-nextBtn relative md:absolute md:right-4 top-0 cursor-pointer inline-block">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy">
                            </div>
                        </div>
                    </div>

                    {{-- <div class="swiper gallery-thumbnails-swiper overflow-hidden md:p-0 z-1 list-none mt-2">
                        <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content">
                            @foreach($property->images as $image)
                                <div class="swiper-slide flex-shrink-0 cursor-pointer">
                                    <img class="gallery-thumb {{ $thumbClasses }}" src="{{ getPropertyImage($image->filepath, 'sm') }}"
                                         data-original="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img" loading="lazy">
                                </div>
                            @endforeach
                        </div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                    </div> --}}
                </div>

                <div class="w-full block xl:hidden pb-8">
                    <div class="grid grid-cols-2 gap-4">
                        <div class="table-cell align-middle">
                            <a href="javascript:" data-target="property-enquiry-form"
                               class="{{ $buttonClasses }} cta cta-text scroll-to hidden lg:block">{{ trans('button.enquire_now') }}</a>
                            <a href="javascript:" data-target="request-details-modal"
                               class="modal-button {{ $buttonClasses }} cta cta-text lg:hidden">{{ trans('button.enquire_now') }}</a>

                        </div>

                        <div class="table-cell align-middle">
                            @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                                @if(user())
                                    @php
                                        $shortlistText = in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())
                                            ? trans('shortlist.saved')
                                            : trans('shortlist.save');

                                        $noFill = !in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray());
                                    @endphp
                                    <a href="{{ localeUrl('/property/'.$property->url_key.'/toggle-shortlist') }}" class="{{ $buttonClasses }}">
                                        <span class="inline-block mr-2">
                                            <img class="svg-inject fill-current stroke-current h-5" src="{{ themeImage('heart.svg') }}" alt="" @if ($noFill) style="fill: none;" @endif>
                                        </span>
                                        <span class="inline-block">{{ $shortlistText }}</span>
                                    </a>
                                @else
                                    <a href="javascript:" data-target="preauth-modal"
                                       class="modal-button {{ $buttonClasses }} flex items-center justify-center">
                                        <span class="inline-block mr-2">
                                            <img class="svg-inject fill-current stroke-current h-5" src="{{ themeImage('heart.svg') }}" alt="" style="fill: none;">
                                        </span>
                                        <span class="inline-block">
                                            {{ Str::limit(trans('shortlist.save'), 14) }}
                                        </span>
                                    </a>
                                @endif
                            @endif
                        </div>

                        @if(user())
                            <div class="table-cell align-middle">
                                <a href="javascript:"
                                   data-target="property-note-modal"
                                   class="modal-button {{ $buttonClasses }} cta cta-text scroll-to">
                                    {{ trans('button.add_property_note') }}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="pt-3">
        <div class="">
            <div class="container px-4 md:px-0 mx-auto pt-8 pb-14">
                <div class="flex flex-wrap">
                    <div class="w-full lg:w-4/6 md:pr-8 lg:pr-8 xl:pr-16">
                        @include(themeViewPath('frontend.components.property.description'))
                    </div>
                    <div class="w-full hidden lg:block lg:w-2/6">
                        @include(themeViewPath('frontend.components.property.features'))

                        <div class="mt-20">
                            @include(themeViewPath('frontend.components.contact-details'))
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="enquire_property" class="bg-whiter">
        <div class="container mx-auto text-center py-12">
            <div id="property-enquiry-form" class="py-8 px-4 lg:px-7 mt-4 rounded-lg">
                <h3 class="header-text text-2xl leading-normal text-center tracking-tight text-primary mb-7">{{ trans('header.enquire_about_this_property') }}</h3>
                <form action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                    <div class="md:grid grid-cols-2 gap-4">
                        <div>
                            <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *"
                                   class="rounded-full bg-white h-14 w-full px-4 mb-3 focus:outline-none" value="{{ optional(user())->name() }}" required>
                            <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *"
                                   class="rounded-full bg-white h-14 w-full px-4 mb-3 focus:outline-none" value="{{ optional(user())->email }}" required>
                            <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }} *"
                                   class="rounded-full bg-white h-14 w-full px-4 mb-3 focus:outline-none" value="{{ optional(user())->tel }}" required>

                            @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-full bg-white h-14 w-full px-4 mb-3 focus:outline-none'])
                        </div>
                        <div>
                            <textarea name="comment" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }} *"
                                      class="rounded-3xl bg-white h-48 w-full px-4 py-3 mb-3 focus:outline-none" required></textarea>
                        </div>

                        <div class="col-span-2">
                            <div class="text-center">
                                <button type="submit"
                                        class="cta text-base text-center tracking-wide font-bold uppercase px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 cta-bg cta-text transition-all">{{ trans('button.send_enquiry') }}</button>
                            </div>

                            @if (shouldShowWhatsappCta())
                                <div class="my-2">
                                    <span class="uppercase">{{ trans('generic.or') }}</span>
                                </div>

                                @include(themeViewPath('frontend.components.whatsapp-cta'), compact('property'))
                            @endif
                        </div>

                        <div class="col-span-2">
                            <div class="border-t mt-4 pt-4">
                                <p class="text-xl leading-loose text-center tracking-tight text-primary mb-2">{{ trans('generic.share_this_property') }}</p>
                                @include(themeViewPath('frontend.components.social.share'))
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.similar_properties'), 'properties' => $relatedProperties])

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Modals --}}

    @if ($property->video_url !== null)
        @include(themeViewPath('frontend.components.modals.video'))
    @endif
    @if ($property->virtual_tour_url !== null)
        @include(themeViewPath('frontend.components.modals.virtual-tour'))
    @endif
    @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
        @include(themeViewPath('frontend.components.modals.location'))
    @endif
    {{-- @if ($property->documents->count() > 0)
        @include(themeViewPath('frontend.components.modals.documents'))
    @endif --}}
    @include(themeViewPath('frontend.components.modals.gallery-overlay'))
    @include(themeViewPath('frontend.components.modals.gallery'))
    @include(themeViewPath('frontend.components.modals.request-details'))
    @include(themeViewPath('frontend.components.modals.book-viewing'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.mortgage-calculator'))
    @include(themeViewPath('frontend.components.modals.stamp-duty-calculator'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.share-this-property'))

    @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
        @include(themeViewPath('frontend.components.modals.brochure-request'))
    @endif

    @if (!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
        @if ($property->documents->count() > 0)
            @include(themeViewPath('frontend.components.modals.documents'))
        @endif
    @else
        @if ($floorplanDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-floorplan-documents-modal', 'documents' => $floorplanDocs])
        @endif

        @if ($epcDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-epc-documents-modal', 'documents' => $epcDocs])
        @endif

        @if ($brochureDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-brochure-documents-modal', 'documents' => $brochureDocs])
        @endif

        @if ($otherDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-other-documents-modal', 'documents' => $otherDocs])
        @endif
    @endif
@endsection
