@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="pt-32">
        <div class="container px-8 xl:px-4 mx-auto">

            <div>
                <h2 class="text-2xl lg:text-3xl pb-2 font-medium py-6 header-text">{{ trans('header.meet_the_team') }}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                <a href="/" class="primary-text">{{ trans('header.home') }}</a> &gt; <a href="{{ localeUrl('/meet-the-team') }}" class="primary-text">{{ trans('header.meet_the_team') }}</a> &gt; {{ $teamMember->name }}
            </div>


            <div class="grid grid-cols-1 lg:grid-cols-4 gap-8">
                <div class="col-span-1">
                    @if ($teamMember->photo)
                        <img src="{{ assetPath($teamMember->photo) }}" class="w-full pb-4 object-cover">
                    @endif
                </div>

                <div class="col-span-1 lg:col-span-3 text-center lg:text-left">
                    <h2 class="text-4xl font-medium">{!!  $teamMember->name  !!}</h2>
                    <span class="block pt-2 text-base font-medium text-gray-800 uppercase">{!!  $teamMember->role  !!}</span>

                    <p class="pt-6">
                        {!! str_replace('<br />', '<br /><br />', $teamMember->description) !!}
                    </p>

                    @if ($teamMember->linked_in !== null || $teamMember->email !== null || $teamMember->tel !== null)
                        <div class="team-member-social-networks mx-auto w-full pt-4">
                            @if($teamMember->linked_in !== null)
                                <div class="rounded-full inline-block primary-bg p-2 mr-2">
                                    <a href="{{ $teamMember->linked_in }}" target="_blank">
                                        <img src="{{ themeImage('icons/social/linkedin.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="LinkedIn" loading="lazy">
                                    </a>
                                </div>
                            @endif
                            @if($teamMember->email !== null)
                                <div class="rounded-full inline-block primary-bg p-2 mr-2">
                                    <a href="mailto:{{ $teamMember->email }}" target="_blank">
                                        <img src="{{ themeImage('icons/social/email.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="Email" loading="lazy">
                                    </a>
                                </div>
                            @endif
                            @if($teamMember->tel !== null)
                                <div class="rounded-full inline-block primary-bg p-2">
                                    <a href="tel:{{ $teamMember->tel }}" target="_blank">
                                        <img src="{{ themeImage('icons/social/phone.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="Phone" loading="lazy">
                                    </a>
                                </div>
                            @endif
                        </div>
                    @endif



                </div>
            </div>
        </div>
    </section>

    <div class="bg-gray-50 mt-8">
    @include(themeViewPath('frontend.components.about'))
    </div>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
