@extends('layouts.app')

@section('content')
    @php
    $boxClasses = 'rounded-lg px-4';
    $submitClasses = 'rounded-lg';
    $boxClasses = 'rounded-lg'
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="instant-valuation" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('instant-valuation.hero') }}">
        <div class="pt-8 pb-28">
            <div class="container mx-auto transparent pt-12 px-8 lg:px-32 xl:px-64 pb-24 text-center">
                <h1 class="text-white text-5xl mx-auto py-6 header-text">{{ trans('valuation.instant_valuation_results') }}</h1>
            </div>
        </div>
    </section>
    <div class="container mx-auto px-8 lg:px-32 xl:px-64 pb-16 -mt-32">
        <div class="bg-white p-12 pb-16 mx-auto text-center shadow-lg border border-gray-100 {{ $boxClasses }}">
            @include(themeViewPath('frontend.components.system-notifications'))

            <h2 class="text-2xl leading-tight tracking-tight text-primary text-center header-text">{{ trans('valuation.average_sale_price_in_your_area') }}</h2>
            @if($averagePrice !== null)
            <p class="text-6xl block mx-auto pt-8 primary-text header-text">&pound;{!! number_format($averagePrice) !!}</p>
            @else
                <p class="text-6xl block mx-auto pt-8 primary-text header-text">Unknown</p>
            @endif
            @if($minPrice !== null || $maxPrice !== null)
                <div class="grid grid-cols-2 text-gray-500 pt-4">
                    <div class="text-right text-lg pr-2 whitespace-nowrap">{{ trans('label.min') }} | &pound;{!! number_format($minPrice) !!}</div>
                    <div class="text-left text-lg pl-2 whitespace-nowrap">{{ trans('label.max') }} | &pound;{!! number_format($maxPrice) !!}</div>
                </div>
            @endif

        </div>
    </div>

    @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
        <div class="container mx-auto text-center px-8">
            <a href="{{ localeUrl('/valuation?lead='.$lead->uuid) }}"
               class="cta text-center px-12 py-4 text-xl leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer {{ $submitClasses }}">
                {{ trans('valuation.book_appointment') }} &gt;
            </a>

            <p class="block pt-8">{{ trans('valuation.book_appointment_text') }}</p>
        </div>
    @endif

    @if($saleData->count() > 0)
        <div class="container mx-auto px-8 pb-16">
            <div class="bg-white p-2 md:p-12 md:pb-16 mx-auto text-center">

                <h2 class="text-2xl leading-loose tracking-tight text-primary text-center header-text pb-12">{{ trans('valuation.historical_purchase_data') }}</h2>

                <table class="table w-full text-left text-gray-600 text-sm">
                    <thead>
                    <tr>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm">Sale Date</th>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm">Address</th>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm hidden md:table-cell">Property Type</th>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm hidden md:table-cell">Tenure</th>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm hidden md:table-cell">New Build</th>
                        <th class="border-b border-t py-6 px-2 dark:border-dark-5 whitespace-nowrap font-normal text-sm">Price</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($saleData as $data)

                        <tr class="hover:bg-gray-100">
                            <td class="py-4 px-2 align-top text-sm">{{ \Carbon\Carbon::parse($data->transfer_date)->format('jS F Y') }}</td>
                            <td class="py-4 px-2 align-top text-sm">{{ $data->displayAddress() }}</td>
                            <td class="py-4 px-2 align-top text-sm hidden md:table-cell">{{ getPropertyTypeFromSaleDataIdentifier($data->property_type) }}</td>
                            <td class="py-4 px-2 align-top text-sm hidden md:table-cell">{{ $data->hold_type === 'F' ? 'Freehold' : 'Leasehold'  }}</td>
                            <td class="py-4 px-2 align-top text-sm hidden md:table-cell">{{ $data->data[5] === 'Y' ? 'Yes' : 'No' }}</td>
                            <td class="py-4 px-2 align-top text-sm">&pound;{{ number_format($data->price) }}</td>

                        </tr>

                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    @else
        <p class="text-center pb-24">Unfortunately there is currently no sale data, please try again.</p>
    @endif


    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
