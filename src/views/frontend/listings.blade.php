@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @include(themeViewPath('frontend.components.search-filter'))
    @include(themeViewPath('frontend.components.search-filter-breadcrumb'))

    <div class="container mx-auto px-4">
        @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])
    </div>

    @php
        $chunks = $properties->chunk(12);
        $cardViewFile = request()->get('list') == true ? 'property-list' : 'property';
        $gridCss = request()->get('list') == true ? 'grid grid-cols-1 gap-4' : 'grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4'
    @endphp

    @if(isset($chunks[0]))
        <section class="mb-6">
            <div class="container mx-auto px-4">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[0] as $i => $property)
                        @include(themeViewPath('frontend.components.cards.' . $cardViewFile))
                        @if (!isset($chunks[1]) && hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_last($chunks[0]->toArray()) - 1))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if (isset($chunks[0], $chunks[1]) && !hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM))
        @include(themeViewPath('frontend.components.listings.listings-divider'))
    @else
        <!-- no properties here -->
    @endif

    @if(isset($chunks[1]))
        <section class="mb-6 mt-6">
            <div class="container mx-auto px-4">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[1] as $i => $property)
                        @include(themeViewPath('frontend.components.cards.' . $cardViewFile))

                        @if (hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_first($chunks[1]->toArray()) + 1))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @include(themeViewPath('frontend.components.listings.listings-pagination'), [
        'data' => $properties,
    ])

    @include(themeViewPath('frontend.components.search-page-content'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Models --}}
    @include(themeViewPath('frontend.components.modals.create-alert'))
    @include(themeViewPath('frontend.components.modals.share-this-search'))

@endsection
