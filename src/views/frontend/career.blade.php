@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @php
        //$multiColumn = hasFeature(\App\Models\TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
        $breadcrumb['/'] = trans('header.home');
        $breadcrumb['/careers'] = trans('career.careers')
    @endphp

    <section id="career-page" class="bg-white bg-whiter pb-12 pt-32 lg:pb-16 lg:pt-48">
        <div class="container px-4 mx-auto ">
            <h1 class="text-2xl lg:text-3xl xl:text-5xl tracking-tight font-bold text-primary pb-2 lg:pb-8 header-text">{{ trans('career.careers') }}</h1>
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])
            <!--  Breadcrumb -->
            <div class="text-sm mb-4">
                @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                   [trans('header.home') => localeUrl('/')],
                   [trans('career.careers') => localeUrl('/careers')],
                   [optional($career->data)->title => null,]
                ]])
            </div>
        </div>
    </section>

    <section class="py-8">
        <div class="container px-4 mx-auto">
            <div class="flex flex-wrap">
                <div class="w-full lg:w-4/6 pr-0 md:pr-8">
                    <div class="mb-16">
                        <h3 class="text-2xl leading-loose tracking-tight font-bold text-primary inline">
                            {!!  optional($career->data)->title  !!}
                            <span class="float-right text-2xl">{{ $career->salary }}</span>
                        </h3>
                        <div class="generic-page-content text-justify">{!! optional($career->data)->description !!}</div>
                    </div>
                </div>
                <div class="w-full lg:w-2/6">
                    <div class="bg-whiter py-9 px-4 lg:px-9 generic-contact-form recaptcha">
                        <h3 class="text-2xl leading-relaxed text-center pb-4 header-text">{{ trans('career.interested') }}</h3>
                        @include(themeViewPath('frontend.forms.career'))
                    </div>
                </div>
            </div>

        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
