document.addEventListener("DOMContentLoaded", function (event) {
    const mobileMenu = document.querySelector(".mobile-menu"),
        header = document.querySelector('#main-nav > div:first-of-type > div:first-of-type');

    document.querySelectorAll(".tham").forEach(el => {
        el.addEventListener("click", function () {
            if (mobileMenu.classList.contains('hidden')) {
                header.classList.add('navigation-bg');
            } else {
                header.classList.remove('navigation-bg');
            }

            mobileMenu.classList.toggle("hidden");
            document.querySelectorAll(".tham").forEach(el => el.classList.toggle('tham-active'));
        });
    });

    new Swiper('.gallery-thumbnails-swiper', {
        loop: true,
        spaceBetween: 1,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true,
        breakpoints: {
            360: {
                slidesPerView: 7,
            },
            1920: {
                slidesPerView: 9,
            },
        },
    });

    const mainNav = document.querySelector('#main-nav'),
        body = document.getElementsByTagName('body')[0],
        stickyNav = document.querySelector('#sticky-nav');

    customScrollBody();

    window.onscroll = function (e) {
        customScrollBody();
    }

    function customScrollBody() {
        if (mainNav && stickyNav && !body.classList.contains('thankyou-index')) {
            const { pageYOffset } = window;
            const { scrollTop, scrollHeight } = mainNav;
            const { offsetHeight } = stickyNav;

            if (pageYOffset >= (scrollTop + scrollHeight)) {
                stickyNav.classList.remove('-top-full', 'opacity-0');
                stickyNav.classList.add('top-0');
                mobileMenu.classList.remove('absolute');
                mobileMenu.classList.add('fixed');
                mobileMenu.style.top = `${offsetHeight}px`;
            } else {
                stickyNav.classList.add('-top-full', 'opacity-0');
                stickyNav.classList.remove('top-0');
                mobileMenu.classList.remove('fixed');
                mobileMenu.classList.add('absolute');
                mobileMenu.style.removeProperty('top');
            }
        }
    }

    document.querySelectorAll('.branch-contacts-toggle').forEach(function (element) {
        element.addEventListener('click', function () {
            const body = document.querySelector('body');
    
            body.style.overflow = 'hidden';
            // body.style.position = 'fixed';
            body.style.width = '100%';
        });
    });

    document.querySelector('.close-branches-dropdown').addEventListener('click', function () {
        const body = document.querySelector('body');

        body.style.overflow = null;
        // body.style.position = null;
        body.style.width = null;
    });

    new Swiper('.testimonial-slider', {
        slidesPerView: 1,
        spaceBetween: 30,
        initialSlide: 1,
        centeredSlides: true,
        loop: true,
        autoplay: true,
        autoHeight: true,
    });
});