<?php

namespace PropertyWebmasters\DiscoverTheme;

use Illuminate\Support\ServiceProvider;

class DiscoverThemeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(__DIR__, 'discover-theme');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/' => base_path('resources/views/vendor/themes/discover/config'),
            __DIR__ . '/views/' => base_path('resources/views/vendor/themes/discover/views'),
        ], 'discover-theme-views');
        
        $this->publishes([
            __DIR__ . '/assets/' => public_path('themes/discover/assets'),
        ], 'discover-theme-assets');
    }
}
